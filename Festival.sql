-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 10, 2017 at 09:52 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Festival`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `compte`
-- (See below for the actual view)
--
CREATE TABLE `compte` (
`pseudo` char(15)
,`password` char(64)
);

-- --------------------------------------------------------

--
-- Table structure for table `participe`
--

CREATE TABLE `participe` (
  `t_invite_inv_inv_id` int(11) NOT NULL,
  `t_evenement_eve_eve_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participe`
--

INSERT INTO `participe` (`t_invite_inv_inv_id`, `t_evenement_eve_eve_id`) VALUES
(1, 1),
(1, 8),
(2, 6),
(2, 10),
(3, 3),
(3, 14),
(4, 5),
(5, 4),
(6, 9),
(7, 11),
(7, 13),
(8, 2),
(9, 12);

-- --------------------------------------------------------

--
-- Table structure for table `t_actualite_act`
--

CREATE TABLE `t_actualite_act` (
  `act_id` int(11) NOT NULL,
  `act_titre` varchar(50) NOT NULL,
  `act_contenu` text NOT NULL,
  `act_media` varchar(500) DEFAULT NULL,
  `act_date` date NOT NULL,
  `act_visible` char(1) NOT NULL,
  `t_organisateur_org_org_pseudo` char(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_actualite_act`
--

INSERT INTO `t_actualite_act` (`act_id`, `act_titre`, `act_contenu`, `act_media`, `act_date`, `act_visible`, `t_organisateur_org_org_pseudo`) VALUES
(1, 'Première programmation dévoilée !', 'Aujourd\'hui, la première version de la programmation à été dévoilée. De nombreux artiste ont répondu à l\'invitation. ', 'style/actu_media.jpeg', '2017-09-21', 'o', 'responsable'),
(2, 'Sortie de la première programmation ', 'La première programmation sera dévoiler le 21/09/17. ', 'style/actu_media.jpeg', '2017-09-15', 'o', 'responsable'),
(3, 'Deuxième version de la programmation ! ', 'Sortie de la deuxième version de la programmation', 'style/actu_media.jpeg', '2017-10-22', 'o', 'responsable');

-- --------------------------------------------------------

--
-- Table structure for table `t_evenement_eve`
--

CREATE TABLE `t_evenement_eve` (
  `eve_id` int(11) NOT NULL,
  `eve_nom` varchar(50) NOT NULL,
  `eve_datedeb` date NOT NULL,
  `eve_datefin` date NOT NULL,
  `eve_descriptif` text NOT NULL,
  `t_lieux_lie_lie_id` int(11) NOT NULL,
  `eve_heuredeb` time NOT NULL,
  `eve_heurefin` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_evenement_eve`
--

INSERT INTO `t_evenement_eve` (`eve_id`, `eve_nom`, `eve_datedeb`, `eve_datefin`, `eve_descriptif`, `t_lieux_lie_lie_id`, `eve_heuredeb`, `eve_heurefin`) VALUES
(1, 'Concert AC/DC', '2018-07-05', '2018-07-05', '', 1, '20:00:00', '22:30:00'),
(2, 'Concert Trust ', '2018-07-05', '2018-07-05', '', 2, '20:00:00', '22:30:00'),
(3, 'Concert The Who', '2018-07-05', '2018-07-05', '', 3, '20:00:00', '22:30:00'),
(4, 'Concert Europe ', '2018-07-06', '2018-07-06', '', 3, '17:00:00', '19:30:00'),
(5, 'Concert Bon Jovi', '2018-07-06', '2018-07-06', '', 2, '17:00:00', '19:30:00'),
(6, 'Concert Scorpions', '2018-07-06', '2018-07-06', '', 1, '20:00:00', '22:30:00'),
(8, 'Séance d\'autographe AC/DC', '2018-07-06', '2018-07-06', '', 4, '14:30:00', '16:30:00'),
(9, 'Concert Kiss', '2018-07-06', '2018-07-06', '', 2, '20:00:00', '22:30:00'),
(10, 'Séance d\'autographe Scorpions', '2018-07-07', '2018-07-07', '', 4, '14:30:00', '16:30:00'),
(11, 'Concert Aerosmith', '2018-07-07', '2018-07-07', '', 2, '17:00:00', '19:30:00'),
(12, 'Concert Iron Maiden', '2018-07-07', '2018-07-07', '', 1, '17:00:00', '19:30:00'),
(13, 'Séance d\'autographe Aerosmith', '2018-07-08', '2018-07-08', '', 4, '14:30:00', '17:30:00'),
(14, 'Séance d\'autographe The Who', '2018-07-05', '2018-07-05', '', 4, '14:30:00', '16:30:00'),
(15, 'Concert de Cloture ', '2018-07-07', '2018-07-07', '', 1, '20:00:00', '23:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_faq_faq`
--

CREATE TABLE `t_faq_faq` (
  `faq_id` int(11) NOT NULL,
  `faq_titre` varchar(50) NOT NULL,
  `faq_question` text NOT NULL,
  `faq_reponse` text,
  `faq_affichage` char(1) DEFAULT NULL,
  `faq_mail_retour` varchar(50) DEFAULT NULL,
  `t_organisateur_org_org_pseudo` char(15) DEFAULT NULL,
  `t_ticket_tic_tic_chaine_car` char(10) DEFAULT NULL,
  `t_ticket_tic_tic_num` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_faq_faq`
--

INSERT INTO `t_faq_faq` (`faq_id`, `faq_titre`, `faq_question`, `faq_reponse`, `faq_affichage`, `faq_mail_retour`, `t_organisateur_org_org_pseudo`, `t_ticket_tic_tic_chaine_car`, `t_ticket_tic_tic_num`) VALUES
(1, 'Logement disponible', 'Y aura t-il des logements disponibles près du festivale ? ', 'Effectivement de nombreux logements seront disponible près du festival. Tout d\'abord, un camping sera disponible pour les festivalier. Vous pourrez trouver aussi des hotels, gîtes... ', 'o', NULL, 'Organisateur1', 'AibUTiQnis', 2),
(2, 'Moyen de restauration sur place. ', 'Y aura-t\'il des moyens de restauration dans ou autour du festival ? ', 'Oui de nombreux moyen de restauration seront disponible dans le festival et même autour du festival. ', 'o', NULL, 'Organisateur2', 'ABgswESydP', 492),
(7, 'Accès handicapé', 'Il y aura-t\'il des installations afin de faciliter l\'accès aux personnes handicapés ?', NULL, NULL, 'kerboit.kevin@gmail.com', NULL, 'aaMiKcHIIF', 321);

-- --------------------------------------------------------

--
-- Table structure for table `t_festival_fes`
--

CREATE TABLE `t_festival_fes` (
  `fes_nom` varchar(50) NOT NULL,
  `fes_lieu` varchar(20) NOT NULL,
  `fes_date_deb` date NOT NULL,
  `fes_date_fin` date NOT NULL,
  `fes_heure_ouvert` time NOT NULL,
  `fes_heure_ferme` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_festival_fes`
--

INSERT INTO `t_festival_fes` (`fes_nom`, `fes_lieu`, `fes_date_deb`, `fes_date_fin`, `fes_heure_ouvert`, `fes_heure_ferme`) VALUES
('Hard Rock\'s Legends', 'Plemet', '2018-07-05', '2018-07-07', '14:00:00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_invite_inv`
--

CREATE TABLE `t_invite_inv` (
  `inv_id` int(11) NOT NULL,
  `inv_nom` varchar(50) NOT NULL,
  `inv_descriptif` text NOT NULL,
  `inv_media` varchar(500) DEFAULT NULL,
  `t_organisateur_org_org_pseudo` char(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_invite_inv`
--

INSERT INTO `t_invite_inv` (`inv_id`, `inv_nom`, `inv_descriptif`, `inv_media`, `t_organisateur_org_org_pseudo`) VALUES
(1, 'AC/DC', 'AC/DC est un groupe de hard rock australo-britannique formé à Sydney en 1973 par les frères Angus et Malcolm Young. Bien que classé dans le hard rock et parfois considéré comme un pionnier du heavy metal , les membres du groupe ont toujours qualifié leur musique de « rock \'n\' roll ». ', 'style/image_inv/ACDC.jpg', 'Organisateur1'),
(2, 'Scorpions', 'Scorpions est un groupe de hard rock allemand, originaire d\'Hanovre. Leur premier album voit le jour en 1972. Le groupe connaît un prestige planétaire – surtout à partir des années 1980 – grâce à des titres hard rock tels que No One Like You en 1982 ou Rock You Like a Hurricane en 1984 et de ballades à l\'instar de Still Loving You la même année ou Wind of Change et Send Me an Angel en 1990, toutes chansons à grand succès commercial. Le déclin médiatique venu à partir des années 1990, les protagonistes se sont tournés vers de nouvelles expériences, avec notamment Moment of Glory (avec l\'Orchestre philharmonique de Berlin) et Acoustica (album live acoustique) mais reviennent à leurs recettes traditionnelles en 2004 avec un Unbreakable salué par de nombreux fans et suivi en 2007 de l\'album Humanity - Hour 1.', 'style/image_inv/scorpions.jpg', 'Organisateur2'),
(3, 'The Who ', 'The Who est un groupe de rock britannique créé à Londres en 1964. Dans sa forme la plus connue et la plus durable, il est composé du chanteur Roger Daltrey, du guitariste Pete Townshend, du bassiste John Entwistle et du batteur Keith Moon.\r\n\r\nPratiquant au départ un rock \'n\' roll explosif, désigné sous le terme de « maximum R&B » et précurseur (après les Kinks de la première période) du mouvement punk, le groupe connut de nombreux autres styles conformément à l\'air du temps : concept album (The Who Sell Out), psychédélique aux paroles décalées (A Quick One While He\'s Away), opéra-rock (Tommy, Quadrophenia), boucles de synthétiseurs (Who\'s Next). Devenus l\'un des symboles des années 1960, les Who ont influencé la musique rock dans son ensemble ; on leur doit des chansons mythiques comme My Generation, Substitute, Pinball Wizard, Behind Blue Eyes, Baba O\'Riley, Won\'t Get Fooled Again, Who Are You ou I Can See for Miles et de nombreux albums consacrés par le public. C\'est l\'un des groupes de rock britannique les plus importants des années 1960-1970, avec notamment les Beatles, Queen, les Rolling Stones, Led Zeppelin, Pink Floyd, les Kinks et Deep Purple, et un des acteurs de la British invasion aux États-Unis.\r\n\r\n', 'style/image_inv/thewho.jpg', 'Organisateur3'),
(4, 'Bon Jovi', 'Bon Jovi est un groupe de rock et heavy metal américain, originaire de Sayreville, dans le New Jersey. Son nom provient de celui du leader et chanteur principal, Jon Bon Jovi (John Francis Bongiovi, Jr.).\r\n\r\nFormé en 1983, le groupe connait un grand succès dans les années 1980, et reste la référence phare du glam metal de la fin des années 1980, en étant l\'un des plus gros vendeurs de disques de l\'époque et l\'un des groupes accomplissant les tournées mondiales les plus imposantes, notamment quant au nombre de dates. Il montre une plus grande longévité que les autres groupes de cette période dite « hair metal » en mélangeant des éléments de hard, de metal, de roots, de blues, de folk , de pop et de country enrichissant sa musique sur le plan artistique et sonore, et se fabriquant ainsi un succès commercial qui dure jusqu\'à aujourd\'hui. Bon Jovi est l\'un des rares groupes à avoir survécu à la vague grunge et alternative du début des années 1990, faisant même en Europe un triomphe aussi important que les représentants du mouvement alternatif et Britpop de l\'époque.', 'style/image_inv/bonjovi.jpg', 'Organisateur4'),
(5, 'Europe', 'Europe est un groupe de metal suédois, originaire de Upplands Väsby, à Stockholm. Depuis sa formation, Europe compte dix albums studio, plusieurs albums live, et de nombreuses compilations best-of.\r\n\r\nAprès deux premiers albums orientés heavy metal rencontrant le succès en Scandinavie et au Japon, au début des années 1980, Europe devient mondialement célèbre avec son troisième album, plus proche désormais du hard rock mélodique et hard FM, rappelant des groupes comme Bon Jovi, The Final Countdown publié en 1986, qui devient un succès considérable et qui se vendit à plus de trois millions d\'exemplaires aux États-Unis, et plus de 20 millions d\'exemplaires dans le monde. Europe devient l\'un des groupes de hard rock les plus couronnés de succès dans les années 1980, tout comme Def Leppard, AC/DC et Bon Jovi, avec plus de 80 millions d\'albums vendus dans le monde. Le groupe atteint le top 20 des albums dans le Billboard 200 avec deux albums (The Final Countdown et Out of This World) et deux singles dans le top 10 dans le Billboard Hot 100 (The Final Countdown et Carrie).', 'style/image_inv/europe.jpg', 'Organisateur5'),
(6, 'Kiss', 'Kiss est un groupe de rock américain originaire de New York. Il est formé en janvier 1973 par le guitariste Paul Stanley (né Stanley Harvey Eisen en 1952) et le bassiste Gene Simmons (Chaim Witz, né en 1949). Très populaire à travers le monde, notamment grâce à leurs maquillages, leurs costumes extravagants, leurs nombreux effets spéciaux sur scène et la célèbre très grande langue de Gene Simmons, Kiss a vendu plus de 40 millions d\'albums aux États-Unis, dont plus de 24 millions ont été certifiés par la RIAA et plus de 150 millions à l\'échelle mondiale : ce qui font de Kiss un des artistes les plus vendeurs de l\'histoire de la musique. Les enregistrements du groupe et de ses membres en solo leur permettent de récolter 30 disques d\'or, 9 disques de platine et 10 disques multi-platine pour les seuls États-Unis. Kiss est souvent reconnu pour ses produits dérivés qui se vendent en quantité industrielle, si bien qu\'en 2006 la « marque » Kiss est évaluée à un milliard de dollars américains.', 'style/image_inv/kiss.jpg', 'Organisateur2'),
(7, 'Aerosmith', 'Aerosmith est un groupe de rock américain, originaire de Boston, dans le Massachusetts, formé en 1970. Ses membres sont parfois appelés « Les Bad Boys de Boston ». Son style hard rock typique des années 1970, enraciné dans le blues, en est venu à également intégrer des éléments de pop, de metal et de rhythm and blues et a inspiré de nombreux artistes de rock et de metal ultérieurs.', 'style/image_inv/aerosmith.jpg', 'Organisateur5'),
(8, 'Trust', 'Trust est un groupe de hard rock et heavy metal français, originaire de Nanterre, dans les Hauts-de-Seine. Il est formé en 1977 et popularisé au début des années 1980. Trust est le seul groupe français de son style à avoir connu un vrai succès populaire, surtout dans les années 1979 à 1983 avec de nombreux passages sur les radios périphériques et même plusieurs invitations à la télévision dans des émissions grand public.', 'style/image_inv/trust.jpg', 'Organisateur3'),
(9, 'Iron Maiden', 'Iron Maiden est un groupe de heavy metal britannique, originaire de Londres. Le groupe est formé en décembre 1975 par le bassiste Steve Harris et rejoint très rapidement par Dave Murray. Il a été parmi les pionniers de ce qui est appelé la New Wave of British Heavy Metal. Iron Maiden représente l\'un des plus grands succès commerciaux du heavy metal, ayant vendu près de 100 millions d\'albums. Leur sens de la mélodie et une certaine complexité les rapprochent du metal progressif.', 'style/image_inv/ironmaiden.jpg', 'Organisateur1');

-- --------------------------------------------------------

--
-- Table structure for table `t_lieux_lie`
--

CREATE TABLE `t_lieux_lie` (
  `lie_id` int(11) NOT NULL,
  `lie_nom` varchar(50) NOT NULL,
  `lie_coordonnees_x` decimal(11,8) NOT NULL,
  `lie_coordonnes_y` decimal(11,8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_lieux_lie`
--

INSERT INTO `t_lieux_lie` (`lie_id`, `lie_nom`, `lie_coordonnees_x`, `lie_coordonnes_y`) VALUES
(1, 'Scène n°1 ', '48.17971980', '-2.58427620'),
(2, 'Scène n°2', '48.17863238', '-2.58406162'),
(3, 'Scène n°3', '48.17927625', '-2.58919001'),
(4, 'Salle signature autographe ', '48.17926409', '-2.59013414'),
(5, 'Camping Municipale de Plemet', '48.17653762', '-2.58737683'),
(6, 'Saint Antoine', '48.17661060', '-2.59403944');

-- --------------------------------------------------------

--
-- Table structure for table `t_objtrouve_obj`
--

CREATE TABLE `t_objtrouve_obj` (
  `obj_id` int(11) NOT NULL,
  `obj_intitule` varchar(50) NOT NULL,
  `obj_description` text,
  `obj_date` date NOT NULL,
  `t_lieux_lie_lie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_objtrouve_obj`
--

INSERT INTO `t_objtrouve_obj` (`obj_id`, `obj_intitule`, `obj_description`, `obj_date`, `t_lieux_lie_lie_id`) VALUES
(1, 'Porte monnaies', 'Porte monnaies bleu avec carte de crédit, carte d\'indentité.', '2018-07-05', 1),
(2, 'Clé retrouver', 'Jeu de clé retrouvé ', '2018-07-05', 4);

-- --------------------------------------------------------

--
-- Table structure for table `t_organisateur_org`
--

CREATE TABLE `t_organisateur_org` (
  `org_pseudo` char(15) NOT NULL,
  `org_mdp` char(64) NOT NULL,
  `org_nom` varchar(20) DEFAULT NULL,
  `org_prenom` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_organisateur_org`
--

INSERT INTO `t_organisateur_org` (`org_pseudo`, `org_mdp`, `org_nom`, `org_prenom`) VALUES
('Organisateur1', 'test2', 'Kerboit', 'Kevin'),
('Organisateur2', 'Org2', 'Toto', 'Michel'),
('Organisateur3', 'Org3', NULL, NULL),
('Organisateur4', 'Org4', NULL, NULL),
('Organisateur5', 'Org5', NULL, NULL),
('Organisateur6', 'Org6', NULL, NULL),
('responsable', 'resp17AGRO!', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_reseausocial_res`
--

CREATE TABLE `t_reseausocial_res` (
  `res_id` int(11) NOT NULL,
  `res_nom` varchar(500) NOT NULL,
  `res_lien` varchar(100) NOT NULL,
  `t_invite_inv_inv_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_reseausocial_res`
--

INSERT INTO `t_reseausocial_res` (`res_id`, `res_nom`, `res_lien`, `t_invite_inv_inv_id`) VALUES
(1, 'Facebook', 'https://www.facebook.com/acdc/', 1),
(2, 'Twitter', 'https://twitter.com/acdc', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_service_ser`
--

CREATE TABLE `t_service_ser` (
  `ser_id` int(11) NOT NULL,
  `ser_type` varchar(15) NOT NULL,
  `ser_nom` varchar(20) NOT NULL,
  `t_lieux_lie_lie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_service_ser`
--

INSERT INTO `t_service_ser` (`ser_id`, `ser_type`, `ser_nom`, `t_lieux_lie_lie_id`) VALUES
(2, 'Camping', 'Camping Municipale', 5),
(3, 'Restaurant ', 'Saint Antoine', 6);

-- --------------------------------------------------------

--
-- Table structure for table `t_ticket_tic`
--

CREATE TABLE `t_ticket_tic` (
  `tic_num` int(11) NOT NULL,
  `tic_chaine_car` char(10) NOT NULL,
  `tic_type` varchar(10) DEFAULT NULL,
  `tic_nom` varchar(20) DEFAULT NULL,
  `tic_prenom` varchar(20) DEFAULT NULL,
  `tic_billeterie` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_ticket_tic`
--

INSERT INTO `t_ticket_tic` (`tic_num`, `tic_chaine_car`, `tic_type`, `tic_nom`, `tic_prenom`, `tic_billeterie`) VALUES
(321, 'aaMiKcHIIF', NULL, NULL, NULL, NULL),
(127, 'aApCEqbmfZ', NULL, NULL, NULL, NULL),
(492, 'ABgswESydP', NULL, NULL, NULL, NULL),
(82, 'aDfwVkKtYU', NULL, NULL, NULL, NULL),
(43, 'AePCATIjRL', NULL, NULL, NULL, NULL),
(188, 'aFKWvXigjE', NULL, NULL, NULL, NULL),
(46, 'AgTHEmglxX', NULL, NULL, NULL, NULL),
(472, 'aHcMzevfyH', NULL, NULL, NULL, NULL),
(232, 'AhstxvyxSU', NULL, NULL, NULL, NULL),
(2, 'AibUTiQnis', NULL, NULL, NULL, NULL),
(438, 'akchdpTCMc', NULL, NULL, NULL, NULL),
(240, 'akEkOfFsat', NULL, NULL, NULL, NULL),
(350, 'aMezEXUDuo', NULL, NULL, NULL, NULL),
(294, 'aNTxnlSCFV', NULL, NULL, NULL, NULL),
(392, 'AptkbpVnZE', NULL, NULL, NULL, NULL),
(63, 'ARhnapAiaH', NULL, NULL, NULL, NULL),
(105, 'AVMuzJnzOA', NULL, NULL, NULL, NULL),
(204, 'AvQDyguvfh', NULL, NULL, NULL, NULL),
(12, 'AZoZBNxLAr', NULL, NULL, NULL, NULL),
(420, 'BAyqjSiVdV', NULL, NULL, NULL, NULL),
(119, 'BCbUDlkVCk', NULL, NULL, NULL, NULL),
(319, 'bdetBtdHQo', NULL, NULL, NULL, NULL),
(372, 'BEjblLbcnn', NULL, NULL, NULL, NULL),
(265, 'berbLSNMNU', NULL, NULL, NULL, NULL),
(457, 'BfCvALhhYh', NULL, NULL, NULL, NULL),
(136, 'BFIIRUmJDs', NULL, NULL, NULL, NULL),
(75, 'bFTOYhYzvx', NULL, NULL, NULL, NULL),
(424, 'bfUsafofwt', NULL, NULL, NULL, NULL),
(117, 'bgAhPmLslE', NULL, NULL, NULL, NULL),
(25, 'bgkKEiHbeO', NULL, NULL, NULL, NULL),
(378, 'bgKQwEHLNL', NULL, NULL, NULL, NULL),
(59, 'BgPcToIaRg', NULL, NULL, NULL, NULL),
(194, 'bLJZhLsyqQ', NULL, NULL, NULL, NULL),
(356, 'BLYprfOGbG', NULL, NULL, NULL, NULL),
(171, 'bmKcEqiOKe', NULL, NULL, NULL, NULL),
(388, 'bOzuoWhuSZ', NULL, NULL, NULL, NULL),
(161, 'BsDyZHNFpF', NULL, NULL, NULL, NULL),
(314, 'bSGVBBsAJe', NULL, NULL, NULL, NULL),
(352, 'BsYERYkvZL', NULL, NULL, NULL, NULL),
(222, 'BWeGSEFQnW', NULL, NULL, NULL, NULL),
(248, 'bzXsVuwVZJ', NULL, NULL, NULL, NULL),
(17, 'cAGffvDefx', NULL, NULL, NULL, NULL),
(425, 'cBEWQUvgaa', NULL, NULL, NULL, NULL),
(456, 'ccGXbTxGyT', NULL, NULL, NULL, NULL),
(165, 'cDFQCzarJX', NULL, NULL, NULL, NULL),
(291, 'cdSbbEFiIj', NULL, NULL, NULL, NULL),
(421, 'CdzuRvPOEw', NULL, NULL, NULL, NULL),
(465, 'ChmmWAoUSd', NULL, NULL, NULL, NULL),
(298, 'ChOMatmxjG', NULL, NULL, NULL, NULL),
(158, 'ciWSCWWlGv', NULL, NULL, NULL, NULL),
(186, 'CjPxYXVWrv', NULL, NULL, NULL, NULL),
(344, 'CleaxIWuRN', NULL, NULL, NULL, NULL),
(81, 'cnlCNNzQtu', NULL, NULL, NULL, NULL),
(278, 'CpObWDZvRQ', NULL, NULL, NULL, NULL),
(36, 'cqXMQLQdTz', NULL, NULL, NULL, NULL),
(168, 'CshGPPZFBn', NULL, NULL, NULL, NULL),
(243, 'CsKxczFbjN', NULL, NULL, NULL, NULL),
(67, 'CVqNxqwoZN', NULL, NULL, NULL, NULL),
(175, 'cwGRdDmtQh', NULL, NULL, NULL, NULL),
(199, 'cWWganqIHh', NULL, NULL, NULL, NULL),
(47, 'CyOcmPfhNo', NULL, NULL, NULL, NULL),
(220, 'daatGCOyNe', NULL, NULL, NULL, NULL),
(29, 'DaxknubMiQ', NULL, NULL, NULL, NULL),
(305, 'DhdNMtOyNY', NULL, NULL, NULL, NULL),
(72, 'dIvhVOBDmF', NULL, NULL, NULL, NULL),
(108, 'dKAOiyOSBc', NULL, NULL, NULL, NULL),
(406, 'DMwJwQXVta', NULL, NULL, NULL, NULL),
(302, 'DOaQPPcMJf', NULL, NULL, NULL, NULL),
(480, 'dqHAhRhSLM', NULL, NULL, NULL, NULL),
(149, 'DRRmHQOWbG', NULL, NULL, NULL, NULL),
(339, 'drWpNMHYHw', NULL, NULL, NULL, NULL),
(153, 'DsNguazhsG', NULL, NULL, NULL, NULL),
(5, 'dSQepsJlAh', NULL, NULL, NULL, NULL),
(343, 'dVldgyoUeN', NULL, NULL, NULL, NULL),
(193, 'DwCHODlFCH', NULL, NULL, NULL, NULL),
(495, 'dWGbQLqFHJ', NULL, NULL, NULL, NULL),
(434, 'dXNfYwZdbn', NULL, NULL, NULL, NULL),
(57, 'EAhToCBZak', NULL, NULL, NULL, NULL),
(258, 'ecsGyrAKdm', NULL, NULL, NULL, NULL),
(55, 'eCyNrxybfS', NULL, NULL, NULL, NULL),
(185, 'EFQshihnIU', NULL, NULL, NULL, NULL),
(113, 'eGcsKImHVY', NULL, NULL, NULL, NULL),
(299, 'EiMgllqvbj', NULL, NULL, NULL, NULL),
(455, 'ejjhvzLYbK', NULL, NULL, NULL, NULL),
(53, 'EMmGpCDLmU', NULL, NULL, NULL, NULL),
(97, 'eNbpojhWpq', NULL, NULL, NULL, NULL),
(50, 'eNgHIYUhXm', NULL, NULL, NULL, NULL),
(463, 'eNvZduZVrf', NULL, NULL, NULL, NULL),
(349, 'eovQDBkXek', NULL, NULL, NULL, NULL),
(1, 'ePsImiecKM', NULL, NULL, NULL, NULL),
(66, 'EToSYRIfXt', NULL, NULL, NULL, NULL),
(121, 'etzjcwnRsF', NULL, NULL, NULL, NULL),
(71, 'eUAbeHnxgb', NULL, NULL, NULL, NULL),
(162, 'ewChirJDSV', NULL, NULL, NULL, NULL),
(34, 'eyJMduUBuY', NULL, NULL, NULL, NULL),
(86, 'EZFKbiZoMf', NULL, NULL, NULL, NULL),
(494, 'FaIKIuaBkf', NULL, NULL, NULL, NULL),
(459, 'FBOOkcnXgl', NULL, NULL, NULL, NULL),
(399, 'fdvKBvZtQg', NULL, NULL, NULL, NULL),
(404, 'fHKSnqCGeg', NULL, NULL, NULL, NULL),
(58, 'fiPodvMkmI', NULL, NULL, NULL, NULL),
(368, 'fpqDgKiWhn', NULL, NULL, NULL, NULL),
(83, 'FqdMbLNPTg', NULL, NULL, NULL, NULL),
(383, 'frxJbUfNHG', NULL, NULL, NULL, NULL),
(101, 'FtTFpgKNgB', NULL, NULL, NULL, NULL),
(262, 'fwwvgWIWTh', NULL, NULL, NULL, NULL),
(309, 'fxagrFSBDA', NULL, NULL, NULL, NULL),
(234, 'fxhHQeXwJZ', NULL, NULL, NULL, NULL),
(205, 'fxLzvDOTOJ', NULL, NULL, NULL, NULL),
(284, 'fykZuxOQUU', NULL, NULL, NULL, NULL),
(337, 'GabigDiyKJ', NULL, NULL, NULL, NULL),
(49, 'GAxXfDEXlJ', NULL, NULL, NULL, NULL),
(393, 'GBBsYMjBzs', NULL, NULL, NULL, NULL),
(432, 'GCeRgSznJR', NULL, NULL, NULL, NULL),
(333, 'gHtwcaCuPk', NULL, NULL, NULL, NULL),
(325, 'GhuBFQhJdI', NULL, NULL, NULL, NULL),
(210, 'GitqQRsgRu', NULL, NULL, NULL, NULL),
(282, 'gJjykNpgTW', NULL, NULL, NULL, NULL),
(397, 'GKuhIkQaoz', NULL, NULL, NULL, NULL),
(500, 'GKUhoMMbAY', NULL, NULL, NULL, NULL),
(460, 'gMUQYCDXNz', NULL, NULL, NULL, NULL),
(400, 'GPyXzUyyZS', NULL, NULL, NULL, NULL),
(301, 'gQHcZftuAn', NULL, NULL, NULL, NULL),
(384, 'GTHjuAMmyM', NULL, NULL, NULL, NULL),
(328, 'gXSmvldnEf', NULL, NULL, NULL, NULL),
(54, 'gyaaPdkWUl', NULL, NULL, NULL, NULL),
(236, 'HbCErreAbk', NULL, NULL, NULL, NULL),
(144, 'HCVlPtpFUt', NULL, NULL, NULL, NULL),
(324, 'hFmFymePDs', NULL, NULL, NULL, NULL),
(245, 'HfQWztsyis', NULL, NULL, NULL, NULL),
(482, 'HFTfRLzIYR', NULL, NULL, NULL, NULL),
(255, 'HGAgUTVwAv', NULL, NULL, NULL, NULL),
(92, 'hiITvRmRYb', NULL, NULL, NULL, NULL),
(91, 'hilFOPsIdo', NULL, NULL, NULL, NULL),
(146, 'hJUXyisOqB', NULL, NULL, NULL, NULL),
(239, 'hlSHlSrwur', NULL, NULL, NULL, NULL),
(453, 'hMNYeDZayd', NULL, NULL, NULL, NULL),
(475, 'hntBMenxhG', NULL, NULL, NULL, NULL),
(173, 'hnWtnUMqcO', NULL, NULL, NULL, NULL),
(6, 'HpubNtTzXt', NULL, NULL, NULL, NULL),
(403, 'hRrrUcdrzy', NULL, NULL, NULL, NULL),
(74, 'HvwzAehjUu', NULL, NULL, NULL, NULL),
(273, 'HYYGItbyfm', NULL, NULL, NULL, NULL),
(126, 'iaidVqGPTr', NULL, NULL, NULL, NULL),
(231, 'iaLcdEGbbf', NULL, NULL, NULL, NULL),
(318, 'iaqKXFNYvV', NULL, NULL, NULL, NULL),
(225, 'iaSyJCWfRw', NULL, NULL, NULL, NULL),
(266, 'IbbrrLTxEd', NULL, NULL, NULL, NULL),
(213, 'ICfVDFIACZ', NULL, NULL, NULL, NULL),
(331, 'iDmKMgKsSR', NULL, NULL, NULL, NULL),
(444, 'IeEzZfMzAs', NULL, NULL, NULL, NULL),
(226, 'IeMPALxdxo', NULL, NULL, NULL, NULL),
(491, 'iiGWMroife', NULL, NULL, NULL, NULL),
(52, 'IjbSWKrIfq', NULL, NULL, NULL, NULL),
(300, 'iJjkoBnldT', NULL, NULL, NULL, NULL),
(304, 'IKDMQdtHAG', NULL, NULL, NULL, NULL),
(433, 'ILFnlAYFte', NULL, NULL, NULL, NULL),
(427, 'ImEMSOQlRa', NULL, NULL, NULL, NULL),
(217, 'IOdnmEmnAR', NULL, NULL, NULL, NULL),
(484, 'iqpyGmAguc', NULL, NULL, NULL, NULL),
(40, 'iqueqmmjyt', NULL, NULL, NULL, NULL),
(80, 'IrDrhwdhMF', NULL, NULL, NULL, NULL),
(9, 'IsWUdviCBy', NULL, NULL, NULL, NULL),
(483, 'iSWyAKOweu', NULL, NULL, NULL, NULL),
(95, 'iuWqiyVmlH', NULL, NULL, NULL, NULL),
(140, 'IvVDdlCGEO', NULL, NULL, NULL, NULL),
(330, 'iZftodcnTy', NULL, NULL, NULL, NULL),
(62, 'jBcRakJmAb', NULL, NULL, NULL, NULL),
(353, 'JBdEnHpdiM', NULL, NULL, NULL, NULL),
(100, 'jBODLoKWrI', NULL, NULL, NULL, NULL),
(85, 'jbwsdWXetZ', NULL, NULL, NULL, NULL),
(458, 'jcyVnoxLHW', NULL, NULL, NULL, NULL),
(288, 'JemjdEXXSW', NULL, NULL, NULL, NULL),
(35, 'JISDrYmHDX', NULL, NULL, NULL, NULL),
(218, 'jmWTUziepy', NULL, NULL, NULL, NULL),
(237, 'JnrIUydIXT', NULL, NULL, NULL, NULL),
(24, 'jnXcsJswlE', NULL, NULL, NULL, NULL),
(64, 'jnZoBvQohv', NULL, NULL, NULL, NULL),
(351, 'JpDQLkNgqF', NULL, NULL, NULL, NULL),
(477, 'jQKrkQIWGL', NULL, NULL, NULL, NULL),
(207, 'jrYcgyvfdP', NULL, NULL, NULL, NULL),
(307, 'JsIGpcvkEL', NULL, NULL, NULL, NULL),
(471, 'jTGJvTytyX', NULL, NULL, NULL, NULL),
(386, 'JuhFuNANqc', NULL, NULL, NULL, NULL),
(118, 'julmqUjhHB', NULL, NULL, NULL, NULL),
(23, 'jUvVNICfEw', NULL, NULL, NULL, NULL),
(214, 'jXiJxmdcIx', NULL, NULL, NULL, NULL),
(485, 'JYTVjsrKvH', NULL, NULL, NULL, NULL),
(73, 'jZsMcxFpps', NULL, NULL, NULL, NULL),
(295, 'kdxkRlSTWD', NULL, NULL, NULL, NULL),
(61, 'KFBJsZeAEl', NULL, NULL, NULL, NULL),
(467, 'kFheLqNiIJ', NULL, NULL, NULL, NULL),
(94, 'kgdMoZlQkv', NULL, NULL, NULL, NULL),
(377, 'KJpJPlWBEq', NULL, NULL, NULL, NULL),
(371, 'KkgSYflsqI', NULL, NULL, NULL, NULL),
(184, 'KKnwubvKUD', NULL, NULL, NULL, NULL),
(109, 'kKxTuvJQkY', NULL, NULL, NULL, NULL),
(28, 'kltXTxwLnj', NULL, NULL, NULL, NULL),
(354, 'KNNUdwGzel', NULL, NULL, NULL, NULL),
(408, 'KoQzBjdkMW', NULL, NULL, NULL, NULL),
(139, 'kOYrOvSinE', NULL, NULL, NULL, NULL),
(241, 'kPaqzEwCiv', NULL, NULL, NULL, NULL),
(335, 'kuArfolifm', NULL, NULL, NULL, NULL),
(416, 'kUSBWCoPfv', NULL, NULL, NULL, NULL),
(410, 'KVZQaeOjBr', NULL, NULL, NULL, NULL),
(141, 'KYbjuTdZmV', NULL, NULL, NULL, NULL),
(445, 'KzoeHUUXFj', NULL, NULL, NULL, NULL),
(362, 'lBCSOeBWtW', NULL, NULL, NULL, NULL),
(124, 'lCeTJrNJkn', NULL, NULL, NULL, NULL),
(30, 'LdPnMFNDRi', NULL, NULL, NULL, NULL),
(268, 'lewPSzUvbd', NULL, NULL, NULL, NULL),
(22, 'LHzOwXIPHJ', NULL, NULL, NULL, NULL),
(271, 'LjHktavvtM', NULL, NULL, NULL, NULL),
(191, 'llrGhTXVle', NULL, NULL, NULL, NULL),
(167, 'lMHioaVABp', NULL, NULL, NULL, NULL),
(154, 'LnFAfEtQcK', NULL, NULL, NULL, NULL),
(203, 'lpAkwuJIkO', NULL, NULL, NULL, NULL),
(235, 'lpBgmgtVTC', NULL, NULL, NULL, NULL),
(125, 'LrgzIkozlQ', NULL, NULL, NULL, NULL),
(355, 'LRLNcHSeVF', NULL, NULL, NULL, NULL),
(311, 'lRWxHQTYPh', NULL, NULL, NULL, NULL),
(429, 'LSBiGknqNT', NULL, NULL, NULL, NULL),
(116, 'lTqGAkhxRb', NULL, NULL, NULL, NULL),
(77, 'LuChNQGCVS', NULL, NULL, NULL, NULL),
(174, 'LuLTlWzPFP', NULL, NULL, NULL, NULL),
(107, 'LuXMAVJglt', NULL, NULL, NULL, NULL),
(340, 'LvjEkiQIyj', NULL, NULL, NULL, NULL),
(275, 'LxprhWLkPq', NULL, NULL, NULL, NULL),
(250, 'LYEFjqRTpi', NULL, NULL, NULL, NULL),
(192, 'LYxpCawDKs', NULL, NULL, NULL, NULL),
(387, 'lyZQZAyvBw', NULL, NULL, NULL, NULL),
(446, 'lZpxYEseLT', NULL, NULL, NULL, NULL),
(417, 'LZtWkiBSMV', NULL, NULL, NULL, NULL),
(33, 'MbpDPmJSwv', NULL, NULL, NULL, NULL),
(201, 'mICXCImooD', NULL, NULL, NULL, NULL),
(264, 'mnrBYPzfER', NULL, NULL, NULL, NULL),
(419, 'MPcaQCqyuD', NULL, NULL, NULL, NULL),
(370, 'MpDBGPAvjF', NULL, NULL, NULL, NULL),
(197, 'mrbTpZsjgX', NULL, NULL, NULL, NULL),
(267, 'MRlksGBihU', NULL, NULL, NULL, NULL),
(169, 'mrZcxQlAVd', NULL, NULL, NULL, NULL),
(200, 'msYfFkLWQC', NULL, NULL, NULL, NULL),
(428, 'mTXQzpCMrn', NULL, NULL, NULL, NULL),
(254, 'mUhLHbpKDR', NULL, NULL, NULL, NULL),
(129, 'MutLBzQMQj', NULL, NULL, NULL, NULL),
(19, 'mxIDZyeWKm', NULL, NULL, NULL, NULL),
(259, 'myZDwdwYKz', NULL, NULL, NULL, NULL),
(289, 'MZbaQJOLoB', NULL, NULL, NULL, NULL),
(375, 'MZkiMkNJds', NULL, NULL, NULL, NULL),
(493, 'MZSNmFMIDN', NULL, NULL, NULL, NULL),
(272, 'nbyByxYOfw', NULL, NULL, NULL, NULL),
(177, 'nCDJJqZUFg', NULL, NULL, NULL, NULL),
(196, 'NdwkNsOYak', NULL, NULL, NULL, NULL),
(56, 'NerXeMZhEk', NULL, NULL, NULL, NULL),
(394, 'nFrmJUDqGL', NULL, NULL, NULL, NULL),
(4, 'NGBDUbgKbm', NULL, NULL, NULL, NULL),
(14, 'NGBsIMZuyC', NULL, NULL, NULL, NULL),
(451, 'NGhLssWujt', NULL, NULL, NULL, NULL),
(88, 'NgXlDhnXYe', NULL, NULL, NULL, NULL),
(0, 'njxLLwjAXg', NULL, NULL, NULL, NULL),
(115, 'nksdamVMqJ', NULL, NULL, NULL, NULL),
(385, 'nLdeaLXyeu', NULL, NULL, NULL, NULL),
(103, 'NLeaJhPisz', NULL, NULL, NULL, NULL),
(178, 'NoDOoRLWlC', NULL, NULL, NULL, NULL),
(79, 'NowfcLZxPV', NULL, NULL, NULL, NULL),
(281, 'nPUsbVOdGn', NULL, NULL, NULL, NULL),
(190, 'NSunFPoJCH', NULL, NULL, NULL, NULL),
(450, 'nTFLXXuTic', NULL, NULL, NULL, NULL),
(402, 'NtLkBsuvmu', NULL, NULL, NULL, NULL),
(313, 'ntpRqBPbRc', NULL, NULL, NULL, NULL),
(357, 'nUMFJOJNgU', NULL, NULL, NULL, NULL),
(363, 'NwHTFKNEYD', NULL, NULL, NULL, NULL),
(133, 'NWzdZSMoKJ', NULL, NULL, NULL, NULL),
(341, 'NzARVFijJw', NULL, NULL, NULL, NULL),
(11, 'NZlqSDSJXy', NULL, NULL, NULL, NULL),
(38, 'oAHwEhKezj', NULL, NULL, NULL, NULL),
(448, 'OAIoagwBlD', NULL, NULL, NULL, NULL),
(401, 'oEIsETWBBF', NULL, NULL, NULL, NULL),
(435, 'ofxxElXBgB', NULL, NULL, NULL, NULL),
(334, 'ogbhguGSwh', NULL, NULL, NULL, NULL),
(156, 'oGqMkLgGnC', NULL, NULL, NULL, NULL),
(312, 'OKoXdOURvL', NULL, NULL, NULL, NULL),
(143, 'OlIxuYHYYH', NULL, NULL, NULL, NULL),
(211, 'oLjwuCSfdT', NULL, NULL, NULL, NULL),
(138, 'OlnDEwElDf', NULL, NULL, NULL, NULL),
(224, 'oLTGQuAWGr', NULL, NULL, NULL, NULL),
(41, 'oOmqOKnnhQ', NULL, NULL, NULL, NULL),
(297, 'ooMrPTrEqM', NULL, NULL, NULL, NULL),
(310, 'OpjhZLtxNw', NULL, NULL, NULL, NULL),
(276, 'OQrNewmWYr', NULL, NULL, NULL, NULL),
(69, 'oRNvYqxcdH', NULL, NULL, NULL, NULL),
(152, 'ORyqAFEiDJ', NULL, NULL, NULL, NULL),
(182, 'OSLkgdCRMr', NULL, NULL, NULL, NULL),
(98, 'OUxeTFFVPs', NULL, NULL, NULL, NULL),
(148, 'ovzUYViAOZ', NULL, NULL, NULL, NULL),
(230, 'owJCzcGuuK', NULL, NULL, NULL, NULL),
(329, 'OxqaLaqRkT', NULL, NULL, NULL, NULL),
(274, 'OyDIwXDbaG', NULL, NULL, NULL, NULL),
(369, 'oZaQHlFfcX', NULL, NULL, NULL, NULL),
(415, 'pbyAIehrTA', NULL, NULL, NULL, NULL),
(413, 'PCpFNNenbs', NULL, NULL, NULL, NULL),
(474, 'PdjanldJfJ', NULL, NULL, NULL, NULL),
(462, 'PEKvKHmmVB', NULL, NULL, NULL, NULL),
(426, 'PEnkkSLBDG', NULL, NULL, NULL, NULL),
(308, 'pFJLFBGVgj', NULL, NULL, NULL, NULL),
(470, 'PgVhbyvnMR', NULL, NULL, NULL, NULL),
(110, 'PLGWfFuFye', NULL, NULL, NULL, NULL),
(104, 'PMaheLeBSt', NULL, NULL, NULL, NULL),
(342, 'pNQtISllwP', NULL, NULL, NULL, NULL),
(317, 'pqwoYqXnsU', NULL, NULL, NULL, NULL),
(390, 'pSGeMcegXt', NULL, NULL, NULL, NULL),
(15, 'PTAjAhOroZ', NULL, NULL, NULL, NULL),
(316, 'pTZErEHsug', NULL, NULL, NULL, NULL),
(251, 'pWgzZfnURF', NULL, NULL, NULL, NULL),
(422, 'pxrKipFNcV', NULL, NULL, NULL, NULL),
(326, 'pXwiqyXtFM', NULL, NULL, NULL, NULL),
(99, 'pYfyQomPyK', NULL, NULL, NULL, NULL),
(364, 'QajEZINKIg', NULL, NULL, NULL, NULL),
(405, 'QbbVoYZgLT', NULL, NULL, NULL, NULL),
(430, 'qhDAKhQrta', NULL, NULL, NULL, NULL),
(227, 'QHgAJDGzHQ', NULL, NULL, NULL, NULL),
(27, 'qhTjGewWDx', NULL, NULL, NULL, NULL),
(279, 'qidhgLeWlF', NULL, NULL, NULL, NULL),
(90, 'QijhzKtKJV', NULL, NULL, NULL, NULL),
(345, 'QiKesRcIGs', NULL, NULL, NULL, NULL),
(391, 'qJNMvfCMfL', NULL, NULL, NULL, NULL),
(128, 'QKHkxnlYBT', NULL, NULL, NULL, NULL),
(44, 'qlzKDdlNGp', NULL, NULL, NULL, NULL),
(120, 'QPMRDeCdrq', NULL, NULL, NULL, NULL),
(418, 'QqcWSOsVZS', NULL, NULL, NULL, NULL),
(39, 'qReLcLPYpj', NULL, NULL, NULL, NULL),
(481, 'qUepagzSWK', NULL, NULL, NULL, NULL),
(380, 'QvoqQHLnii', NULL, NULL, NULL, NULL),
(452, 'QXFcwVYCON', NULL, NULL, NULL, NULL),
(464, 'qxkBfUznAP', NULL, NULL, NULL, NULL),
(8, 'QXoJHRaOGF', NULL, NULL, NULL, NULL),
(106, 'qYGQNUfpzn', NULL, NULL, NULL, NULL),
(290, 'RAGqXtImuX', NULL, NULL, NULL, NULL),
(423, 'RBeQYMPfjp', NULL, NULL, NULL, NULL),
(142, 'rEKtOWNWwB', NULL, NULL, NULL, NULL),
(180, 'RfExpRWSze', NULL, NULL, NULL, NULL),
(31, 'rgmTKztCaS', NULL, NULL, NULL, NULL),
(454, 'rgWKBXBlmt', NULL, NULL, NULL, NULL),
(360, 'RIOOAEoyeN', NULL, NULL, NULL, NULL),
(179, 'RKhdaohkuW', NULL, NULL, NULL, NULL),
(7, 'rNMPkCDoXA', NULL, NULL, NULL, NULL),
(221, 'RPscuprTXA', NULL, NULL, NULL, NULL),
(215, 'RrHNoJtXqw', NULL, NULL, NULL, NULL),
(327, 'RSAoPQNeBy', NULL, NULL, NULL, NULL),
(3, 'rsoyPwNQoS', NULL, NULL, NULL, NULL),
(440, 'RssnEbLBZx', NULL, NULL, NULL, NULL),
(398, 'rThyyKRLdt', NULL, NULL, NULL, NULL),
(45, 'ruDOczpMZy', NULL, NULL, NULL, NULL),
(114, 'rVUCMcoNMH', NULL, NULL, NULL, NULL),
(447, 'rwjckBIcjq', NULL, NULL, NULL, NULL),
(176, 'rzBoBCAMdj', NULL, NULL, NULL, NULL),
(111, 'sCXFJrnXXY', NULL, NULL, NULL, NULL),
(469, 'seChbBLSIm', NULL, NULL, NULL, NULL),
(26, 'SEEjJSJLjD', NULL, NULL, NULL, NULL),
(487, 'SeZntYhDVD', NULL, NULL, NULL, NULL),
(137, 'SicYSJazFd', NULL, NULL, NULL, NULL),
(473, 'sLyUpnpnYh', NULL, NULL, NULL, NULL),
(431, 'sNEUjDOmhl', NULL, NULL, NULL, NULL),
(466, 'SoDLTYAIua', NULL, NULL, NULL, NULL),
(439, 'sOjevDPUnB', NULL, NULL, NULL, NULL),
(209, 'SqvcjIHXMl', NULL, NULL, NULL, NULL),
(436, 'stHxLQlapo', NULL, NULL, NULL, NULL),
(102, 'sVDjQvpNzb', NULL, NULL, NULL, NULL),
(68, 'swvdEoVrwH', NULL, NULL, NULL, NULL),
(151, 'syKMNxJfFT', NULL, NULL, NULL, NULL),
(374, 'sYNmETQKGK', NULL, NULL, NULL, NULL),
(414, 'sZLDmpNOYK', NULL, NULL, NULL, NULL),
(198, 'sZYqUtwvay', NULL, NULL, NULL, NULL),
(242, 'SZYyrbZzuZ', NULL, NULL, NULL, NULL),
(479, 'TAnkkmpQOE', NULL, NULL, NULL, NULL),
(238, 'tBWiOGDyDz', NULL, NULL, NULL, NULL),
(32, 'tDkPxouQGW', NULL, NULL, NULL, NULL),
(202, 'tGMYVZmTKj', NULL, NULL, NULL, NULL),
(292, 'ThCXJWffys', NULL, NULL, NULL, NULL),
(320, 'THgVuhTpzA', NULL, NULL, NULL, NULL),
(60, 'ThQmuRchIm', NULL, NULL, NULL, NULL),
(21, 'TiHjtLBxFA', NULL, NULL, NULL, NULL),
(373, 'tkBNbgOULM', NULL, NULL, NULL, NULL),
(476, 'tmjfPVJJks', NULL, NULL, NULL, NULL),
(365, 'ttojhYfSjr', NULL, NULL, NULL, NULL),
(407, 'TuajuUecxQ', NULL, NULL, NULL, NULL),
(442, 'TUeqdgXFrC', NULL, NULL, NULL, NULL),
(37, 'TUOeOSidJF', NULL, NULL, NULL, NULL),
(261, 'TUtlLqRBaf', NULL, NULL, NULL, NULL),
(252, 'TvjBWMyBjP', NULL, NULL, NULL, NULL),
(296, 'tXdAQRYlIW', NULL, NULL, NULL, NULL),
(332, 'TXLCfQtIet', NULL, NULL, NULL, NULL),
(338, 'TZtGViBUqN', NULL, NULL, NULL, NULL),
(488, 'UARysvJHnZ', NULL, NULL, NULL, NULL),
(212, 'UcgIHBnyZv', NULL, NULL, NULL, NULL),
(396, 'uCJlIrPsFV', NULL, NULL, NULL, NULL),
(323, 'uDDEUQYxXT', NULL, NULL, NULL, NULL),
(157, 'uGXGTBtBSG', NULL, NULL, NULL, NULL),
(269, 'UJscUllxOn', NULL, NULL, NULL, NULL),
(315, 'ukHPOdAAtb', NULL, NULL, NULL, NULL),
(293, 'UlmhroInoY', NULL, NULL, NULL, NULL),
(244, 'UmmCqSvTTX', NULL, NULL, NULL, NULL),
(497, 'UmNhQHjFYH', NULL, NULL, NULL, NULL),
(150, 'umtugzNAIl', NULL, NULL, NULL, NULL),
(195, 'UPLHGDTigL', NULL, NULL, NULL, NULL),
(96, 'UrnJdIgLPw', NULL, NULL, NULL, NULL),
(270, 'uUymfGgVmr', NULL, NULL, NULL, NULL),
(366, 'uvhiUDcGvI', NULL, NULL, NULL, NULL),
(163, 'uWDmeQrSWj', NULL, NULL, NULL, NULL),
(155, 'uXFBADAEun', NULL, NULL, NULL, NULL),
(361, 'uyzEHsmXLu', NULL, NULL, NULL, NULL),
(145, 'UZphaXgUZT', NULL, NULL, NULL, NULL),
(228, 'VaCOqqnoUS', NULL, NULL, NULL, NULL),
(219, 'VAYYbdqtbV', NULL, NULL, NULL, NULL),
(229, 'VBZAuQuDgP', NULL, NULL, NULL, NULL),
(381, 'veJyckXBUz', NULL, NULL, NULL, NULL),
(76, 'vfKGQfJcIy', NULL, NULL, NULL, NULL),
(347, 'vfLzWoLDFR', NULL, NULL, NULL, NULL),
(358, 'VgoTXyHvMu', NULL, NULL, NULL, NULL),
(443, 'VIrIsIhloZ', NULL, NULL, NULL, NULL),
(134, 'VLeHWUPuxz', NULL, NULL, NULL, NULL),
(42, 'vpzkpxQncg', NULL, NULL, NULL, NULL),
(249, 'vSgWsnrlYn', NULL, NULL, NULL, NULL),
(286, 'VtjqaooEoH', NULL, NULL, NULL, NULL),
(112, 'vtvwgMoTDD', NULL, NULL, NULL, NULL),
(183, 'vtZzHiSpuD', NULL, NULL, NULL, NULL),
(10, 'vVhuTHrpnT', NULL, NULL, NULL, NULL),
(348, 'vwGHkZcWlB', NULL, NULL, NULL, NULL),
(490, 'vztYOtnuOV', NULL, NULL, NULL, NULL),
(322, 'VZvkuTiMUG', NULL, NULL, NULL, NULL),
(499, 'VZziajPXKu', NULL, NULL, NULL, NULL),
(336, 'WBRFRDKjrm', NULL, NULL, NULL, NULL),
(13, 'WBRQFhgKpi', NULL, NULL, NULL, NULL),
(147, 'WIASSInMzs', NULL, NULL, NULL, NULL),
(172, 'wJqlKeyEVq', NULL, NULL, NULL, NULL),
(170, 'WJSKGbKlDO', NULL, NULL, NULL, NULL),
(216, 'WLgWLwblEz', NULL, NULL, NULL, NULL),
(166, 'wLnupyKICb', NULL, NULL, NULL, NULL),
(89, 'wMyqCIAYgc', NULL, NULL, NULL, NULL),
(208, 'woLlMvQeFw', NULL, NULL, NULL, NULL),
(496, 'WqAheUaHUh', NULL, NULL, NULL, NULL),
(257, 'WqPGRlCCGE', NULL, NULL, NULL, NULL),
(382, 'wRayOBHyCb', NULL, NULL, NULL, NULL),
(287, 'WRSHfsiBVd', NULL, NULL, NULL, NULL),
(277, 'WUTAPalglD', NULL, NULL, NULL, NULL),
(159, 'wwwBUoXNoc', NULL, NULL, NULL, NULL),
(246, 'xBfzzGTlFs', NULL, NULL, NULL, NULL),
(486, 'XCVrIBoRCY', NULL, NULL, NULL, NULL),
(256, 'xePRNYdvzs', NULL, NULL, NULL, NULL),
(247, 'xEuIhODQQl', NULL, NULL, NULL, NULL),
(16, 'XEXnthGicS', NULL, NULL, NULL, NULL),
(20, 'XhekVpLdRu', NULL, NULL, NULL, NULL),
(93, 'xhQjdtakcO', NULL, NULL, NULL, NULL),
(411, 'xiuZqFDsfV', NULL, NULL, NULL, NULL),
(346, 'XKGvEEZfbh', NULL, NULL, NULL, NULL),
(389, 'XKTMmmQZxJ', NULL, NULL, NULL, NULL),
(65, 'XKUEGHnakV', NULL, NULL, NULL, NULL),
(131, 'xLOyxiiOCX', NULL, NULL, NULL, NULL),
(263, 'xMRODBwaSB', NULL, NULL, NULL, NULL),
(376, 'XoVICKssYP', NULL, NULL, NULL, NULL),
(135, 'xOVxxSiFLE', NULL, NULL, NULL, NULL),
(123, 'XRHxzweEDn', NULL, NULL, NULL, NULL),
(78, 'xrRqzzTaXj', NULL, NULL, NULL, NULL),
(498, 'xSgdJmAahm', NULL, NULL, NULL, NULL),
(122, 'xtVnLFExcR', NULL, NULL, NULL, NULL),
(437, 'XtyJjMCuWn', NULL, NULL, NULL, NULL),
(359, 'xuZIzKfOYS', NULL, NULL, NULL, NULL),
(489, 'xvmBZLaQGI', NULL, NULL, NULL, NULL),
(132, 'YaJTpSMkkq', NULL, NULL, NULL, NULL),
(260, 'yaqXwhbIzn', NULL, NULL, NULL, NULL),
(468, 'yCmdlvAHbV', NULL, NULL, NULL, NULL),
(412, 'YEJDLCbcYW', NULL, NULL, NULL, NULL),
(441, 'YGetADGDgZ', NULL, NULL, NULL, NULL),
(280, 'YgjYbDFeNp', NULL, NULL, NULL, NULL),
(303, 'YjGupyQYMf', NULL, NULL, NULL, NULL),
(181, 'yLBZLNYHzq', NULL, NULL, NULL, NULL),
(283, 'yLKVJuYtyv', NULL, NULL, NULL, NULL),
(367, 'yMJuraaPUJ', NULL, NULL, NULL, NULL),
(306, 'YmNRWHlsyZ', NULL, NULL, NULL, NULL),
(130, 'ymTdoOtKUx', NULL, NULL, NULL, NULL),
(253, 'YNRPHpEwDG', NULL, NULL, NULL, NULL),
(48, 'yoRVihAqJG', NULL, NULL, NULL, NULL),
(160, 'YqZTdScDTx', NULL, NULL, NULL, NULL),
(223, 'YrAzkXEmGX', NULL, NULL, NULL, NULL),
(233, 'ytYpAlLzBg', NULL, NULL, NULL, NULL),
(164, 'yvHjChhSZt', NULL, NULL, NULL, NULL),
(395, 'YxKrtyFsvB', NULL, NULL, NULL, NULL),
(449, 'YxqhawcsaI', NULL, NULL, NULL, NULL),
(285, 'zaeZravvoL', NULL, NULL, NULL, NULL),
(187, 'ZAGFhGZjjk', NULL, NULL, NULL, NULL),
(84, 'zcerhArmCU', NULL, NULL, NULL, NULL),
(189, 'ZFJzDEYRVO', NULL, NULL, NULL, NULL),
(70, 'ZKdTGpewfI', NULL, NULL, NULL, NULL),
(51, 'zmHinNryjd', NULL, NULL, NULL, NULL),
(206, 'znzMSovnWD', NULL, NULL, NULL, NULL),
(461, 'ZOBKGhWpUZ', NULL, NULL, NULL, NULL),
(409, 'ZsfFvDaKkz', NULL, NULL, NULL, NULL),
(478, 'ZuSFrIvICq', NULL, NULL, NULL, NULL),
(379, 'ZUWtjaCTQn', NULL, NULL, NULL, NULL),
(18, 'zVTbLMhzdb', NULL, NULL, NULL, NULL),
(87, 'ZYdvfgtdkx', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure for view `compte`
--
DROP TABLE IF EXISTS `compte`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `compte`  AS  select `t_organisateur_org`.`org_pseudo` AS `pseudo`,`t_organisateur_org`.`org_mdp` AS `password` from `t_organisateur_org` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `participe`
--
ALTER TABLE `participe`
  ADD PRIMARY KEY (`t_invite_inv_inv_id`,`t_evenement_eve_eve_id`),
  ADD KEY `participe_eve_fk` (`t_evenement_eve_eve_id`);

--
-- Indexes for table `t_actualite_act`
--
ALTER TABLE `t_actualite_act`
  ADD PRIMARY KEY (`act_id`),
  ADD KEY `act_org_fk` (`t_organisateur_org_org_pseudo`);

--
-- Indexes for table `t_evenement_eve`
--
ALTER TABLE `t_evenement_eve`
  ADD PRIMARY KEY (`eve_id`),
  ADD KEY `eve_lie_fk` (`t_lieux_lie_lie_id`);

--
-- Indexes for table `t_faq_faq`
--
ALTER TABLE `t_faq_faq`
  ADD PRIMARY KEY (`faq_id`),
  ADD KEY `faq_org_fk` (`t_organisateur_org_org_pseudo`),
  ADD KEY `faq_tic_fk` (`t_ticket_tic_tic_chaine_car`,`t_ticket_tic_tic_num`);

--
-- Indexes for table `t_festival_fes`
--
ALTER TABLE `t_festival_fes`
  ADD PRIMARY KEY (`fes_nom`);

--
-- Indexes for table `t_invite_inv`
--
ALTER TABLE `t_invite_inv`
  ADD PRIMARY KEY (`inv_id`),
  ADD KEY `inv_org_fk` (`t_organisateur_org_org_pseudo`);

--
-- Indexes for table `t_lieux_lie`
--
ALTER TABLE `t_lieux_lie`
  ADD PRIMARY KEY (`lie_id`);

--
-- Indexes for table `t_objtrouve_obj`
--
ALTER TABLE `t_objtrouve_obj`
  ADD PRIMARY KEY (`obj_id`),
  ADD KEY `obj_lie_fk` (`t_lieux_lie_lie_id`);

--
-- Indexes for table `t_organisateur_org`
--
ALTER TABLE `t_organisateur_org`
  ADD PRIMARY KEY (`org_pseudo`);

--
-- Indexes for table `t_reseausocial_res`
--
ALTER TABLE `t_reseausocial_res`
  ADD PRIMARY KEY (`res_id`),
  ADD KEY `res_inv_fk` (`t_invite_inv_inv_id`);

--
-- Indexes for table `t_service_ser`
--
ALTER TABLE `t_service_ser`
  ADD PRIMARY KEY (`ser_id`),
  ADD KEY `ser_lie_fk` (`t_lieux_lie_lie_id`);

--
-- Indexes for table `t_ticket_tic`
--
ALTER TABLE `t_ticket_tic`
  ADD PRIMARY KEY (`tic_chaine_car`,`tic_num`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_actualite_act`
--
ALTER TABLE `t_actualite_act`
  MODIFY `act_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_evenement_eve`
--
ALTER TABLE `t_evenement_eve`
  MODIFY `eve_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `t_faq_faq`
--
ALTER TABLE `t_faq_faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_invite_inv`
--
ALTER TABLE `t_invite_inv`
  MODIFY `inv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `t_lieux_lie`
--
ALTER TABLE `t_lieux_lie`
  MODIFY `lie_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_objtrouve_obj`
--
ALTER TABLE `t_objtrouve_obj`
  MODIFY `obj_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_reseausocial_res`
--
ALTER TABLE `t_reseausocial_res`
  MODIFY `res_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_service_ser`
--
ALTER TABLE `t_service_ser`
  MODIFY `ser_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `participe`
--
ALTER TABLE `participe`
  ADD CONSTRAINT `participe_eve_fk` FOREIGN KEY (`t_evenement_eve_eve_id`) REFERENCES `t_evenement_eve` (`eve_id`),
  ADD CONSTRAINT `participe_inv_fk` FOREIGN KEY (`t_invite_inv_inv_id`) REFERENCES `t_invite_inv` (`inv_id`);

--
-- Constraints for table `t_actualite_act`
--
ALTER TABLE `t_actualite_act`
  ADD CONSTRAINT `act_org_fk` FOREIGN KEY (`t_organisateur_org_org_pseudo`) REFERENCES `t_organisateur_org` (`org_pseudo`);

--
-- Constraints for table `t_evenement_eve`
--
ALTER TABLE `t_evenement_eve`
  ADD CONSTRAINT `eve_lie_fk` FOREIGN KEY (`t_lieux_lie_lie_id`) REFERENCES `t_lieux_lie` (`lie_id`);

--
-- Constraints for table `t_faq_faq`
--
ALTER TABLE `t_faq_faq`
  ADD CONSTRAINT `faq_org_fk` FOREIGN KEY (`t_organisateur_org_org_pseudo`) REFERENCES `t_organisateur_org` (`org_pseudo`),
  ADD CONSTRAINT `faq_tic_fk` FOREIGN KEY (`t_ticket_tic_tic_chaine_car`,`t_ticket_tic_tic_num`) REFERENCES `t_ticket_tic` (`tic_chaine_car`, `tic_num`);

--
-- Constraints for table `t_invite_inv`
--
ALTER TABLE `t_invite_inv`
  ADD CONSTRAINT `inv_org_fk` FOREIGN KEY (`t_organisateur_org_org_pseudo`) REFERENCES `t_organisateur_org` (`org_pseudo`);

--
-- Constraints for table `t_objtrouve_obj`
--
ALTER TABLE `t_objtrouve_obj`
  ADD CONSTRAINT `obj_lie_fk` FOREIGN KEY (`t_lieux_lie_lie_id`) REFERENCES `t_lieux_lie` (`lie_id`);

--
-- Constraints for table `t_reseausocial_res`
--
ALTER TABLE `t_reseausocial_res`
  ADD CONSTRAINT `res_inv_fk` FOREIGN KEY (`t_invite_inv_inv_id`) REFERENCES `t_invite_inv` (`inv_id`);

--
-- Constraints for table `t_service_ser`
--
ALTER TABLE `t_service_ser`
  ADD CONSTRAINT `ser_lie_fk` FOREIGN KEY (`t_lieux_lie_lie_id`) REFERENCES `t_lieux_lie` (`lie_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
