<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Desc_invite extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('db_model');
    $this->load->helper('url_helper');
  }

  public function index(){
    redirect('Invite/afficher');
  }

  public function afficher($IDinv=null){

    if($IDinv==null){
      redirect('Invite/afficher');
    }
        $data['inv']=$this->db_model->get_one_invite($IDinv);
        $data['res']=$this->db_model->get_ressocial($IDinv);

        $this->load->view('templates/header');
        $this->load->view('desc_invite_afficher',$data);
        $this->load->view('templates/footer');
  }

}
?>
