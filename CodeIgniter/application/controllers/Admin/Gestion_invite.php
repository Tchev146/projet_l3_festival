<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gestion_invite extends CI_Controller {
        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
          $this->load->library('session');
          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
        }

        public function index(){
          $this->afficher();
        }

        public function afficher(){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            $data['titre']="Bonjour ".$this->session->userdata('login');
            $data['compte']=$this->db_model->get_all_invite();
            $this->load->view('admin/templates/header.php', $data);
            $this->load->view('admin/gestion_invites.php', $data);
            $this->load->view('admin/templates/footer.php', $data);
          }
        }

        public function modifier($IDinv=null){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            if($IDinv==null){
              redirect('Admin/Accueil_admin');
            }

            $this->form_validation->set_rules('invi_name', 'nom', 'trim|required');
            $this->form_validation->set_rules('invi_desc', 'description', 'trim|required');
            $this->form_validation->set_rules('invi_media', 'média', 'trim|callback_test_file');
            $this->form_validation->set_rules('invi_org', 'organisateur', 'trim|required');

            if($this->form_validation->run()){
              $donnes = array(
                'inv_nom'=>$this->input->post('invi_name'),
                'inv_descriptif'=>$this->input->post('invi_desc'),
                'inv_media'=>$this->input->post('invi_media'),
                't_organisateur_org_org_pseudo'=>$this->input->post('invi_org'),
              );

              $this->db_model->update_inv($donnes, $IDinv);

              $data['success']="Mise à jour réussie";
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['inv']=$this->db_model->get_one_invite($IDinv);
              $data['org']=$this->db_model->get_all_compte();

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/modif_inv.php', $data);
              $this->load->view('form/modif_inv_form.php', $data);
              $this->load->view('admin/templates/footer.php', $data);

            }
            else{
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['inv']=$this->db_model->get_one_invite($IDinv);
              $data['org']=$this->db_model->get_all_compte();

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/modif_inv.php', $data);
              $this->load->view('form/modif_inv_form.php', $data);
              $this->load->view('admin/templates/footer.php', $data);
            }

          }
        }

        public function ajouter(){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            $this->form_validation->set_rules('invi_name', 'nom', 'trim|required');
            $this->form_validation->set_rules('invi_desc', 'description', 'trim|required');
            $this->form_validation->set_rules('invi_media', 'média', 'trim|callback_test_file');
            $this->form_validation->set_rules('invi_org', 'organisateur', 'trim|required');

            if($this->form_validation->run()){
              $donnes = array(
                'inv_nom'=>$this->input->post('invi_name'),
                'inv_descriptif'=>$this->input->post('invi_desc'),
                'inv_media'=>$this->input->post('invi_media'),
                't_organisateur_org_org_pseudo'=>$this->input->post('invi_org'),
              );

              $this->db_model->insert_inv($donnes);

              $data['success']="Insertion dans la base réussie";
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['org']=$this->db_model->get_all_compte();

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/insertion_inv.php', $data);
              $this->load->view('form/insertion_inv_form.php', $data);
              $this->load->view('admin/templates/footer.php', $data);

            }
            else{
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['org']=$this->db_model->get_all_compte();

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/insertion_inv.php', $data);
              $this->load->view('form/insertion_inv_form.php', $data);
              $this->load->view('admin/templates/footer.php', $data);
            }
          }
        }

        public function suppression($IDinv=null){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            if($IDinv==null){
              redirect('Admin/Accueil_admin');
            }

            $this->db_model->delete_inv($IDinv);
            redirect('Admin/Gestion_invite/afficher');
          }
        }

        public function test_file(){
          if(!file_exists($this->input->post('invi_media'))==1 && $this->input->post('invi_media')!=NULL){
              $this->form_validation->set_message('test_file', 'Ce fichier n\'existe pas !');
              return FALSE;
          }
          else{
            return TRUE;
          }
        }

}
