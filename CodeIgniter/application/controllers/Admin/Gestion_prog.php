<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gestion_prog extends CI_Controller {
        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
          $this->load->library('session');
          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
        }

        public function index(){
          $this->afficher();
        }

        public function afficher(){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            $data['titre']="Bonjour ".$this->session->userdata('login');
            $data['eve']=$this->db_model->get_all_evenement();
            $data['model']=$this->db_model;

            $this->load->view('admin/templates/header.php', $data);
            $this->load->view('admin/gestion_eve.php', $data);
            $this->load->view('admin/templates/footer.php', $data);
          }
        }

        public function modifier($IDeve=null){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            if($IDeve==null){
              redirect('Admin/Accueil_admin');
            }

            $this->form_validation->set_rules('eve_nom', 'nom événement', 'trim|required');
            $this->form_validation->set_rules('eve_desc', 'description', 'trim|required');
            //$this->form_validation->set_rules('inv_nom', 'nom invité', 'trim|required');
            $this->form_validation->set_rules('lie_nom', 'nom lieu', 'trim|required');
            $this->form_validation->set_rules('eve_datedeb', 'date début', 'trim|required');
            $this->form_validation->set_rules('eve_datefin', 'date fin', 'trim|required');
            $this->form_validation->set_rules('eve_heuredeb', 'heure début', 'trim|required');
            $this->form_validation->set_rules('eve_heurefin', 'heure fin', 'trim|required');

            if($this->form_validation->run()){

              $rese = $this->db_model->select_id_lie($this->input->post('lie_nom'));

              $donnes = array(
                'eve_id'=>$IDeve,
                'eve_nom'=>$this->input->post('eve_nom'),
                'eve_descriptif'=>$this->input->post('eve_desc'),
                'eve_datedeb'=>$this->input->post('eve_datedeb'),
                'eve_datefin'=>$this->input->post('eve_datefin'),
                't_lieux_lie_lie_id'=>$rese->lie_id,
                'eve_heuredeb'=>$this->input->post('eve_heuredeb'),
                'eve_heurefin'=>$this->input->post('eve_heurefin')
              );

              $this->db_model->delete_paticipe($IDeve);

              $this->db_model->insert_prog($donnes);

              $eve = $this->db_model->select_id_eve($this->input->post('eve_nom'));

              $invite_arr = array();
              $cpt=1;
              while($this->input->post('inv_n'.$cpt)!=null){
                array_push($invite_arr, $this->input->post('inv_n'.$cpt));
                $cpt++;
              }

              foreach($invite_arr as $key){
                $inv = $this->db_model->select_id_inv($key);
                $this->db_model->insert_participe($eve->eve_id, $inv->inv_id);
              }

              $data['success']="Mise à jour réussie";
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['eve']=$this->db_model->get_one_evenement($IDeve);
              $data['inv_eve']=$this->db_model->get_inv_in_eve($IDeve);
              $data['inv']=$this->db_model->get_all_invite();
              $data['lie']=$this->db_model->get_all_lieux();

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/modif_eve.php', $data);
              $this->load->view('form/modif_eve_form.php', $data);
              $this->load->view('admin/templates/footer.php', $data);
           }
           else{
             $data['titre']="Bonjour ".$this->session->userdata('login');
             $data['eve']=$this->db_model->get_one_evenement($IDeve);
             $data['inv_eve']=$this->db_model->get_inv_in_eve($IDeve);
             $data['inv']=$this->db_model->get_all_invite();
             $data['lie']=$this->db_model->get_all_lieux();

             $this->load->view('admin/templates/header.php', $data);
             $this->load->view('admin/modif_eve.php', $data);
             $this->load->view('form/modif_eve_form.php', $data);
             $this->load->view('admin/templates/footer.php', $data);
           }

          }
        }

        public function ajouter(){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            $this->form_validation->set_rules('eve_nom', 'nom événement', 'trim|required');
            $this->form_validation->set_rules('eve_desc', 'description', 'trim|required');
            //$this->form_validation->set_rules('inv_nb"', 'nombre invité', 'required');
            $this->form_validation->set_rules('lie_nom', 'nom lieu', 'trim|required');
            $this->form_validation->set_rules('eve_datedeb', 'date début', 'trim|required');
            $this->form_validation->set_rules('eve_datefin', 'date fin', 'trim|required');
            $this->form_validation->set_rules('eve_heuredeb', 'heure début', 'trim|required');
            $this->form_validation->set_rules('eve_heurefin', 'heure fin', 'trim|required');

            if($this->form_validation->run()){

              $res = $this->db_model->select_id_lie($this->input->post('lie_nom'));

              $donnes = array(
                'eve_nom'=>$this->input->post('eve_nom'),
                'eve_descriptif'=>$this->input->post('eve_desc'),
                'eve_datedeb'=>$this->input->post('eve_datedeb'),
                'eve_datefin'=>$this->input->post('eve_datefin'),
                't_lieux_lie_lie_id'=>$res->lie_id,
                'eve_heuredeb'=>$this->input->post('eve_heuredeb'),
                'eve_heurefin'=>$this->input->post('eve_heurefin')
              );

              $this->db_model->insert_prog($donnes);

              $eve = $this->db_model->select_id_eve($this->input->post('eve_nom'));

              $invite_arr = array();
              $cpt=0;
              while($this->input->post('inv_n'.$cpt)!=null){
                array_push($invite_arr, $this->input->post('inv_n'.$cpt));
                $cpt++;
              }

              foreach($invite_arr as $key){
                $inv = $this->db_model->select_id_inv($key);
                $this->db_model->insert_participe($eve->eve_id, $inv->inv_id);
              }


              $data['success']="Insertion réussie";
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['inv']=$this->db_model->get_all_invite();
              $data['lie']=$this->db_model->get_all_lieux();

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/insertion_eve.php', $data);
              $this->load->view('form/insertion_eve_form.php', $data);
              $this->load->view('admin/templates/footer.php', $data);

          }
          else{
            $data['titre']="Bonjour ".$this->session->userdata('login');
            $data['inv']=$this->db_model->get_all_invite();
            $data['lie']=$this->db_model->get_all_lieux();

            $this->load->view('admin/templates/header.php', $data);
            $this->load->view('admin/insertion_eve.php', $data);
            $this->load->view('form/insertion_eve_form.php', $data);
            $this->load->view('admin/templates/footer.php', $data);
          }
        }
      }

        public function suppression($IDeve){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            if($IDeve==null){
              redirect('Admin/Accueil_admin');
            }
            $this->db_model->delete_paticipe($IDeve);
            redirect('Admin/Gestion_prog/afficher');
          }
        }
}
