<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gestion_FAQ extends CI_Controller {
        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
          $this->load->library('session');
          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
        }

        public function index(){
          $this->afficher();
        }

        public function afficher(){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            $data['titre']="Bonjour ".$this->session->userdata('login');
            $data['questions']=$this->db_model->get_all_questions();
            $this->load->view('admin/templates/header.php', $data);
            $this->load->view('admin/gestion_faq.php', $data);
            $this->load->view('admin/templates/footer.php', $data);
          }
        }

        public function modifier($IDfaq=null){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            if($IDfaq==null){
              redirect('Admin/Accueil_admin');
            }

            $this->form_validation->set_rules('ques_titre', 'titre', 'trim|required');
						$this->form_validation->set_rules('ques_question', 'question', 'trim|required');
            $this->form_validation->set_rules('ques_rep', 'reponse', 'trim');
						$this->form_validation->set_rules('ques_mail', 'adresse mail', 'trim|valid_email');
            $this->form_validation->set_rules('ques_org', 'organisateur', 'trim|required');
            $this->form_validation->set_rules('ques_aff', 'affichage', 'trim|required');
						$this->form_validation->set_rules('ques_num_tic', 'numéro de ticket', 'trim|required|callback_num_check');
						$this->form_validation->set_rules('ques_chaine', 'chaine de caractères', 'trim|required|callback_char_check');

						if($this->form_validation->run()){
							$donnes= array(
								'faq_titre'=>$this->input->post('ques_titre'),
								'faq_question'=>$this->input->post('ques_question'),
                'faq_reponse'=>$this->input->post('ques_rep'),
								'faq_mail_retour'=>$this->input->post('ques_mail'),
                't_organisateur_org_org_pseudo'=>$this->input->post('ques_org'),
								'faq_affichage'=>$this->input->post('ques_aff'),
								't_ticket_tic_tic_num'=>$this->input->post('ques_num_tic'),
								't_ticket_tic_tic_chaine_car'=>$this->input->post('ques_chaine')
							);

              $this->db_model->update_faq($donnes, $IDfaq);

              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['question']=$this->db_model->get_one_question($IDfaq);
              $data['org']=$this->db_model->get_all_compte();
              $data['success']="Mise à jour réussie";

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/modif_faq.php', $data);
              $this->load->view('form/modif_faq_form.php', $data);
              $this->load->view('admin/templates/footer.php', $data);
          }
          else{
            $data['titre']="Bonjour ".$this->session->userdata('login');
            $data['question']=$this->db_model->get_one_question($IDfaq);
            $data['org']=$this->db_model->get_all_compte();

            $this->load->view('admin/templates/header.php', $data);
            $this->load->view('admin/modif_faq.php', $data);
            $this->load->view('form/modif_faq_form.php', $data);
            $this->load->view('admin/templates/footer.php', $data);
          }
        }
      }

      public function suppression($IDfaq=null){
        if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
          redirect('Connexion/login');
        }
        else{
          if($IDfaq==null){
            redirect('Admin/Accueil_admin');
          }

          $this->db_model->delete_faq($IDfaq);
          redirect('Admin/Gestion_FAQ/afficher');
        }
      }

      public function num_check($num){
        if($num == null){
          $this->form_validation->set_message('num_check', 'The numéro de ticket field is required');
          return false;
        }
        $check = $this->db_model->check_num_tic($num);
        if($check==null){
          $this->form_validation->set_message('num_check', 'Le numéro de ticket entré n\'exite pas');
          return FALSE;
        }
        else return TRUE;
      }

      public function char_check(){
        $num=$this->input->post('ques_num_tic');
        $char=$this->input->post('ques_chaine');
        if($num == null && $char==null){
          $this->form_validation->set_message('ques_chaine', 'The chaine de caractères field is required');
          return FALSE;
        }
        $check = $this->db_model->check_num_char_tic($num, $char);
        if($check==null){
          $this->form_validation->set_message('char_check', 'Le numéros de ticket et la chaine de caracères entré ne correspondent pas');
          return FALSE;
        }
        else return TRUE;
      }
}
