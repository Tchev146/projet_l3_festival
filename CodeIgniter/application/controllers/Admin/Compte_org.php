<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Compte_org extends CI_Controller {
        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
          $this->load->library('session');
        }

        public function index(){
          $this->afficher();
        }

        public function afficher(){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            $data['titre']="Bonjour ".$this->session->userdata('login');
            $data['compte']=$this->db_model->get_all_compte();
            $this->load->view('admin/templates/header.php', $data);
            $this->load->view('admin/compte_org_afficher', $data);
            $this->load->view('admin/templates/footer.php', $data);
          }
        }
}
