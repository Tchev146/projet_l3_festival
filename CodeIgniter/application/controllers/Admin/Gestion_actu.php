<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gestion_actu extends CI_Controller {
        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
          $this->load->library('session');
          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
        }

        public function index(){
          $this->afficher();
        }

        public function afficher(){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{

            $data['titre']="Bonjour ".$this->session->userdata('login');
            $data['actu']=$this->db_model->get_ttes_actualite();

            $this->load->view('admin/templates/header.php', $data);
            $this->load->view('admin/gestion_actu.php', $data);
            $this->load->view('admin/templates/footer.php', $data);
          }
        }

        public function modifier($IDactu=null){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{
            if($IDactu==null){
              redirect('Admin/Accueil_admin');
            }

            $this->form_validation->set_rules('actu_titre', 'titre', 'trim|required');
            $this->form_validation->set_rules('actu_contenue', 'contenu', 'trim|required');
            $this->form_validation->set_rules('actu_media', 'média', 'trim|callback_test_file');
            $this->form_validation->set_rules('actu_org', 'organisateur', 'trim|required');
            $this->form_validation->set_rules('actu_aff', 'affichage', 'trim|required');
            $this->form_validation->set_rules('actu_date', 'date', 'trim|required');

            if($this->form_validation->run()){

              $donnes = array(
                'act_titre'=>$this->input->post('actu_titre'),
                'act_contenu'=>$this->input->post('actu_contenue'),
                'act_media'=>$this->input->post('actu_media'),
                't_organisateur_org_org_pseudo'=>$this->input->post('actu_org'),
                'act_visible'=>$this->input->post('actu_aff'),
                'act_date'=>$this->input->post('actu_date')
              );

              $this->db_model->update_actu($donnes, $IDactu);

              $data['success']="Mise à jour réussie";
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['actu']=$this->db_model->get_one_actu($IDactu);
              $data['org']=$this->db_model->get_all_compte();

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/modif_actu.php', $data);

              $this->load->view('form/modif_actu_form.php', $data);
              $this->load->view('admin/templates/footer.php', $data);

            }
            else{
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['actu']=$this->db_model->get_one_actu($IDactu);
              $data['org']=$this->db_model->get_all_compte();

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/modif_actu.php', $data);

              $this->load->view('form/modif_actu_form.php', $data);
              $this->load->view('admin/templates/footer.php', $data);
            }
        }
      }

      public function ajouter(){
        if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
          redirect('Connexion/login');
        }
        else{
          $this->form_validation->set_rules('actu_titre', 'titre', 'trim|required');
          $this->form_validation->set_rules('actu_contenue', 'contenu', 'trim|required');
          $this->form_validation->set_rules('actu_media', 'média', 'trim|callback_test_file');
          $this->form_validation->set_rules('actu_org', 'organisateur', 'trim|required');
          $this->form_validation->set_rules('actu_aff', 'affichage', 'trim|required');
          $this->form_validation->set_rules('actu_date', 'date', 'trim|required');

          if($this->form_validation->run()){
            $donnes = array(
              'act_titre'=>$this->input->post('actu_titre'),
              'act_contenu'=>$this->input->post('actu_contenue'),
              'act_media'=>$this->input->post('actu_media'),
              't_organisateur_org_org_pseudo'=>$this->input->post('actu_org'),
              'act_visible'=>$this->input->post('actu_aff'),
              'act_date'=>$this->input->post('actu_date')
            );

            $this->db_model->insert_actu($donnes);

            $data['success']="Insertion dans la base réussie";
            $data['titre']="Bonjour ".$this->session->userdata('login');
            $data['org']=$this->db_model->get_all_compte();

            $this->load->view('admin/templates/header.php', $data);
            $this->load->view('admin/insertion_actu.php', $data);

            $this->load->view('form/insertion_actu_form.php', $data);
            $this->load->view('admin/templates/footer.php', $data);

          }
          else{
            $data['titre']="Bonjour ".$this->session->userdata('login');
            $data['org']=$this->db_model->get_all_compte();

            $this->load->view('admin/templates/header.php', $data);
            $this->load->view('admin/insertion_actu.php', $data);

            $this->load->view('form/insertion_actu_form.php', $data);
            $this->load->view('admin/templates/footer.php', $data);
          }
        }
      }

      public function suppression($IDactu=null){
        if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
          redirect('Connexion/login');
        }
        else{
          if($IDactu==null){
            redirect('Admin/Accueil_admin');
          }

          $this->db_model->delete_actu($IDactu);
          redirect('Admin/Gestion_actu/afficher');
        }
      }

      public function test_file(){
        if(!file_exists($this->input->post('actu_media'))==1){
            $this->form_validation->set_message('test_file', 'Ce fichier n\'existe pas !');
            return FALSE;
        }
        else{
          return TRUE;
        }
      }
}
