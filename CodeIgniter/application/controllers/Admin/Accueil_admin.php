<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Accueil_admin extends CI_Controller {
        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
          $this->load->library('session');
          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
        }

        public function index(){
          $this->afficher();
        }

        public function afficher(){
          if(!$this->session->userdata('login') || !$this->session->userdata('logged')){
            redirect('Connexion/login');
          }
          else{

            $this->form_validation->set_rules('username', 'utilisateur', 'trim');
            $this->form_validation->set_rules('password-set', 'mot de passe actuel', 'trim|required');
            $this->form_validation->set_rules('exampleInputName', 'nom', 'trim');
            $this->form_validation->set_rules('exampleInputFirstName', 'prenom', 'trim');
            $this->form_validation->set_rules('new-password', 'nouveau mot de passe', 'trim');
            $this->form_validation->set_rules('new-password-confirm', 'confirmation mot de passe', 'trim');

            if($this->form_validation->run()){
             if(!$this->input->post('new-password')==null && !$this->input->post('new-password-confirm')==null){
                $np = $this->input->post('new-password');
                $npc = $this->input->post('new-password-confirm');
                if($np == $npc){
                  $password = $np;
                }
                else{
                  $data["error"]="Les deux mot de passe ne correspondent pas ! ";
                  $data['titre']="Bonjour ".$this->session->userdata('login');
                  $data['org']=$this->db_model->get_one_compte($this->session->userdata('login'));
                  $this->load->view('admin/templates/header.php', $data);
                  $this->load->view('admin/accueil_admin', $data);
                  $this->load->view('form/form_compte_logger.php', $data);
                  $this->load->view('admin/templates/footer.php', $data);
                  return;
                }
              }
              else{
                $pass = hash("sha256", $this->input->post('password-set')."JemetduSel");
                $verife = $this->db_model->check_compte($this->session->userdata('login'), $pass);
                if(!$verife==null){
                  $password=$this->input->post('password-set');
                }
                else{
                  $data["error_pa"]="Mot de passe Incorect";
                  $data['titre']="Bonjour ".$this->session->userdata('login');
                  $data['org']=$this->db_model->get_one_compte($this->session->userdata('login'));
                  $this->load->view('admin/templates/header.php', $data);
                  $this->load->view('admin/accueil_admin', $data);
                  $this->load->view('form/form_compte_logger.php', $data);
                  $this->load->view('admin/templates/footer.php', $data);
                  return;
                }

              }

              $pass_avec_sel=$password."JemetduSel";
              $pass_encrypt=hash("sha256", $pass_avec_sel);

              $donnes=array(
                'org_pseudo'=>$this->input->post('username'),
                'org_mdp'=>$pass_encrypt,
                'org_nom'=>$this->input->post('exampleInputLastName'),
                'org_prenom'=>$this->input->post('exampleInputName')
              );

              $this->db_model->update_org($donnes, $this->session->userdata('login'));

              $data['success']="Mise à jour réussie";
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['org']=$this->db_model->get_one_compte($this->session->userdata('login'));

              $sess = array('login'=>$this->input->post('username'), 'logged'=>true);
              $this->session->set_userdata($sess);

              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/accueil_admin', $data);
              $this->load->view('form/form_compte_logger.php', $data);
              $this->load->view('admin/templates/footer.php', $data);


            }
            else{
              $data['titre']="Bonjour ".$this->session->userdata('login');
              $data['org']=$this->db_model->get_one_compte($this->session->userdata('login'));
              $this->load->view('admin/templates/header.php', $data);
              $this->load->view('admin/accueil_admin', $data);
              $this->load->view('form/form_compte_logger.php', $data);
              $this->load->view('admin/templates/footer.php', $data);
            }
          }
        }
}
