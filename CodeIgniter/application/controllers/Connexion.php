<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Connexion extends CI_Controller {
        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
          $this->load->library('session');
        }

        public function index(){
						$this->login();
				}

        public function login(){
          if($this->session->userdata('login') || $this->session->userdata('logged')){
            redirect('Admin/Accueil_admin/afficher');
          }
          else{
              $this->load->helper(array('form', 'url'));
              $this->load->library('form_validation');

              $this->form_validation->set_rules('username', 'utilisateur', 'trim|required',
              	array('required' => 'Vous devez rentrer un utilisateur.')
              );

              $this->form_validation->set_rules('password', 'mot de passe', 'trim|required',
                array('required' => 'Vous devez rentrer un mot de passe.')
              );

              if($this->form_validation->run()){
                $pseudo=$this->input->post('username');
                $pass=$this->input->post('password');


                $pass = hash("sha256", $pass."JemetduSel");

                $verife = $this->db_model->check_compte($pseudo,$pass);
                if(!$verife==null){
                  $donnes = array('login'=>$pseudo, 'logged'=>true);
                  $this->session->set_userdata($donnes);
                  redirect('Admin/Accueil_admin/afficher');
                }
                else{
                  $data['error']="Identifiant ou mot de passe inconnu ! ";
                  $this->load->view('form/login_form',$data);
                }

              }
              else{
                $this->load->view('form/login_form');
              }
            }
      }

      public function logout(){
        $this->session->unset_userdata('login');
        $this->session->unset_userdata('logged');
        $this->session->sess_destroy();
        redirect(site_url());
      }
}
