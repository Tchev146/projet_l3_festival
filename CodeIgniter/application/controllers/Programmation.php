<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Programmation extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('db_model');
    $this->load->helper('url_helper');
  }

  public function index(){
    afficher();
  }

  public function afficher(){
      $data['titre']="Programmation : ";
      $data['prog']=$this->db_model->get_all_evenement();

      $this->load->view('templates/header');
      $this->load->view('programmation_afficher',$data);
      $this->load->view('templates/footer');
  }
}
?>
