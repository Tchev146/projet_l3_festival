<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Actualite extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('db_model');
    $this->load->helper('url_helper');
  }

  public function index(){
      $this->afficher();
  }

  public function afficher(){
      $data['titre']="Actualite : ";
      $data['actu']=$this->db_model->get_all_actualite();

      $this->load->view('templates/header');
      $this->load->view('actualite_afficher',$data);
      $this->load->view('templates/footer');
  }
}
?>
