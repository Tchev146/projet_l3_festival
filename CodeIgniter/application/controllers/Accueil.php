<?php


	defined('BASEPATH') OR exit('No direct script access allowed');
    class Accueil extends CI_Controller{

			  public function index(){
						$this->afficher();
				}

        public function afficher(){

						$data['titre'] = 'Info du festival :';
						$data['infos'] = $this->db_model->get_info_fest();


  					$this->load->vars($data);
            //Chargement de la vu header.php
            $this->load->view('templates/header');

            //Chargement de la vu page_accueil.php
            $this->load->view('page_accueil');

            //Chargement de la vu footer.php
            $this->load->view('templates/footer');
        }

				public function __construct(){
			    parent::__construct();
			    $this->load->model('db_model');
			    $this->load->helper('url_helper');
			  }


    }
?>
