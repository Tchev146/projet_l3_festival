<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
    class Faq extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
					$this->load->helper('form');
					$this->load->library('form_validation');
        }

				public function index(){
						$this->afficher();
				}

        public function afficher(){
						$this->form_validation->set_rules('faq_titre', 'titre', 'trim|required');
						$this->form_validation->set_rules('faq_question', 'question', 'trim|required');
						$this->form_validation->set_rules('faq_mail_retour', 'adresse mail', 'trim|valid_email');
						$this->form_validation->set_rules('tic_num', 'numéro de ticket', 'trim|required|callback_num_check');
						$this->form_validation->set_rules('tic_chaine_car', 'chaine de caractères', 'trim|required|callback_char_check');

						if($this->form_validation->run()){
							$data = array(
								'faq_titre'=>$this->input->post('faq_titre'),
								'faq_question'=>$this->input->post('faq_question'),
								'faq_mail_retour'=>$this->input->post('faq_mail_retour'),
								'faq_affichage'=>"n",
								't_ticket_tic_tic_num'=>$this->input->post('tic_num'),
								't_ticket_tic_tic_chaine_car'=>$this->input->post('tic_chaine_car')
							);

							$this->db_model->add_faq($data);

							$data['titre'] = 'Foire aux questions :';
							$data['question'] = $this->db_model->get_all_faq();
							$data['success'] = "Votre question à bien été envoyée";
							$this->load->view('templates/header');
	            $this->load->view('faq_afficher', $data);
							$this->load->view('form/faq_form', $data);
	            $this->load->view('templates/footer');

						}
						else{
							$data['titre'] = 'Foire aux questions :';
							$data['question'] = $this->db_model->get_all_faq();

							$this->load->view('templates/header');
	            $this->load->view('faq_afficher', $data);
							$this->load->view('form/faq_form', $data);
	            $this->load->view('templates/footer');
						}


        }

				public function num_check($num=null){
					if($num == null){
						$this->form_validation->set_message('num_check', 'The numéro de ticket field is required');
						return false;
					}
					$check = $this->db_model->check_num_tic($num);
					if($check==null){
						$this->form_validation->set_message('num_check', 'Le numéro de ticket entré n\'exite pas');
						return FALSE;
					}
					else return TRUE;
				}

				public function char_check(){
					$num=$this->input->post('tic_num');
					$char=$this->input->post('tic_chaine_car');
					if($num == null && $char==null){
						$this->form_validation->set_message('char_check', 'The chaine de caractères field is required');
						return FALSE;
					}
					$check = $this->db_model->check_num_char_tic($num, $char);
					if($check==null){
						$this->form_validation->set_message('char_check', 'Le numéro de ticket et la chaine de caracères entré ne correspondent pas');
						return FALSE;
					}
					else return TRUE;
				}

    }
?>
