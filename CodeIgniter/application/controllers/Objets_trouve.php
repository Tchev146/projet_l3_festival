<?php


	defined('BASEPATH') OR exit('No direct script access allowed');
    class Objets_trouve extends CI_Controller{

        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
        }

				public function index(){
						$this->afficher();
				}

        public function afficher(){

						$data['titre'] = 'Liste des objets trouvés :';
						$data['obj'] = $this->db_model->get_all_obj();


            $this->load->view('templates/header');
            $this->load->view('obj_afficher', $data);
            $this->load->view('templates/footer');
        }


    }
?>
