<?php


	defined('BASEPATH') OR exit('No direct script access allowed');
    class Lieux extends CI_Controller{

        public function __construct(){
          parent::__construct();
          $this->load->model('db_model');
          $this->load->helper('url_helper');
        }

				public function index(){
						$this->afficher();
				}

        public function afficher(){

						$data['titre'] = 'Liste des lieux au seins du festival :';
						$data['lieux'] = $this->db_model->get_all_lieux();


            $this->load->view('templates/header');
            $this->load->view('lieux_afficher', $data);
            $this->load->view('templates/footer');
        }


    }
?>
