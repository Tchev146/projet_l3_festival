<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Invite extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('db_model');
    $this->load->helper('url_helper');
  }

  public function index(){
      $this->afficher();
  }

  public function afficher(){
      $data['titre']="Invités : ";
      $data['inv']=$this->db_model->get_all_invite();

      $this->load->view('templates/header');
      $this->load->view('invite_afficher',$data);
      $this->load->view('templates/footer');
  }
}
?>
