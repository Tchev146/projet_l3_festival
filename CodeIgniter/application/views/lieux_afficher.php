<main class="main-content">
				<div class="fullwidth-block inner-content">
					<div class="container">
						<div class="row">
							<div class="col-md-7">
								<div class="content">
									<br />
									<br />
									<h2 class="entry-title"><?php echo $titre;?></h2>

									<?php foreach ($lieux as $donnees) :?>
									<div class="post">
										<h2 class="entry-title"><?php echo $donnees['lie_nom'];?></h2>
										<p>Coordonnées du lieu : <br /> X : <?php echo $donnees['lie_coordonnees_x'];?> <br /> Y : <?php echo $donnees['lie_coordonnes_y'];?></p>
									</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>
				</div>
</main><!-- .main-content -->
