<main class="main-content">
				<div class="fullwidth-block inner-content">
					<div class="container">
						<div class="row">
							<div class="col-md-7">
								<div class="content">
                  <br />
                  <br />
									<h2 class="entry-title"><?php echo $inv->inv_nom;?></h2>
                  <?php
                    $path = base_url().$inv->inv_media;
                  ?>
									<figure class="featured-image">
										<img src="<?php echo $path;?>" alt="<?php echo $inv->inv_nom."_image";?> ">
									</figure>
									<p class="leading"><?php echo $inv->inv_descriptif;?></p>
									<p>Ecrit par : <?php echo $inv->t_organisateur_org_org_pseudo;?></p>
								</div>
							</div>
							<div class="col-md-4 col-md-push-1">
								<aside class="sidebar">
									<div class="widget">
										<br />
	                  <br />
										<h3 class="widget-title">Réseau Social</h3>
										<ul class="discography-list">
											<?php foreach ($res as $key): ?>
												<li>
													<figure class="cover"><a href="<?php echo $key['res_lien'];?>"><img src="<?php echo base_url();?>style/image_res/<?php echo $key['res_nom'];?>-logo.png" alt=""></a></figure>
													<div class="detail">
														<h3><a href="<?php echo $key['res_lien'];?>"><?php echo $key['res_nom'];?></a></h3>
													</div>
												</li>
											<?php endforeach;?>
										</ul>
									</div>
								</aside>
							</div>
            </div>
          </div>
        </div>
</main>
