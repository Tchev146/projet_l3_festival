<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        Back-Office
      </li>
      <li class="breadcrumb-item active">Gestion Invités</li>
    </ol>

    <h1><?php echo $titre; ?></h1>
    <br />

    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Liste des invités
        <a href="<?php echo site_url('Admin/Gestion_invite/ajouter');?>" ><img style="float: right" src="<?php echo base_url();?>style/ic_add_black_48dp_1x.png" ></a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead><tr><td>Nom : </td><td>Organisateur : </td><td>Modifier ? </td></tr></thead>
            <tbody>
              <?php
                foreach ($compte as $key) {
                  echo('<tr><td>'.$key['inv_nom'].'</td><td>'.$key['t_organisateur_org_org_pseudo'].'</td><td><a href="'.site_url("Admin/Gestion_invite/modifier/".$key['inv_id']).'">Cliquez ici</a></td></tr>');
                }
              ?>
            </tbody>
        </table>
      </div>
    </div>
  </div>
