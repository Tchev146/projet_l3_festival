  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          Back-Office
        </li>
        <li class="breadcrumb-item active">Compte Organisateur</li>
      </ol>

      <h1><?php echo $titre; ?></h1>
      <br />

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Liste des comptes organisateur</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead><tr><td>Pseudo</td></tr></thead>
              <tbody>
                <?php
                  foreach ($compte as $key) {
                    echo('<tr><td>'.$key['org_pseudo'].'</td></tr>');
                  }
                ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
