<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        Back-Office
      </li>
      <li class="breadcrumb-item active">Gestion FAQ</li>
    </ol>


    <h1><?php echo $titre; ?></h1>
    <br />

    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Liste des questions</div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead><tr><td>Titre : </td><td>Question : </td><td>Reponse : </td><td>Organisateur : </td><td>Adresse Mail : </td><td>Affichage : </td><td>N° de ticket : </td><td>Chaine de charactère : </td></tr></thead>
            <tbody>
              <?php
                foreach ($questions as $key) {
                  if($key['faq_affichage']=='o'){
                    $aff="Oui";
                  }
                  else{
                    $aff="Non";
                  }
                  echo('<tr><td><a href="'.site_url("Admin/Gestion_FAQ/modifier/".$key['faq_id']).'">'.$key['faq_titre'].'</a></td><td>'.$key['faq_question'].'</td><td>'.$key['faq_reponse'].'</td><td>'.$key['t_organisateur_org_org_pseudo'].'</td><td>'.$key['faq_mail_retour'].'</td><td>'.$aff.'</td><td>'.$key['t_ticket_tic_tic_num'].'</td><td>'.$key['t_ticket_tic_tic_chaine_car'].'</td></tr>');
                }
              ?>
            </tbody>
        </table>
      </div>
    </div>
  </div>
