<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        Back-Office
      </li>
      <li class="breadcrumb-item active">Gestion des événements</li>
    </ol>


    <h1><?php echo $titre; ?></h1>
    <br />

    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Liste des événements
        <a href="<?php echo site_url('Admin/Gestion_prog/ajouter');?>" ><img style="float: right" src="<?php echo base_url();?>style/ic_add_black_48dp_1x.png" ></a>
      </div>

      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead><tr><td>Nom : </td><td>Lieu : </td><td>Invité : </td><td>Date : </td><td>Heure début :</td><td>Heure fin : </td></tr></thead>
            <tbody>
              <?php
                $old = 0;
                foreach ($eve as $key) {
                    $nbInv = $model->get_nb_inv_par_eve($key['eve_id']);
                    if($key['eve_id']!=$old){
                      if($nbInv->nb_invite_eve!=1){
                        echo('<tr><td><a href="'.site_url("Admin/Gestion_prog/modifier/".$key['eve_id']).'">'.$key['eve_nom'].'</a></td><td>'.$key['lie_nom'].'</td><td>'.$nbInv->nb_invite_eve.'</td><td>'.$key['eve_datedeb'].'</td><td>'.$key['eve_heuredeb'].'</td><td>'.$key['eve_heurefin'].'</td></tr>');
                        $old=$key['eve_id'];
                      }
                      else{
                        echo('<tr><td><a href="'.site_url("Admin/Gestion_prog/modifier/".$key['eve_id']).'">'.$key['eve_nom'].'</a></td><td>'.$key['lie_nom'].'</td><td>'.$key['inv_nom'].'</td><td>'.$key['eve_datedeb'].'</td><td>'.$key['eve_heuredeb'].'</td><td>'.$key['eve_heurefin'].'</td></tr>');
                        $old=$key['eve_id'];
                      }

                    }
                }
              ?>
            </tbody>
        </table>
      </div>
    </div>
  </div>
