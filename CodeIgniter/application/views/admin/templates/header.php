<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Hard Rock's Legends : Page d'administration</title>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>style/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>style/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>style/admin/css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="">Hard Rock's Legends : Page d'administration</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="<?php echo site_url("Admin/Accueil_admin/afficher")?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Accueil</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Compte Organisateur">
          <a class="nav-link" href="<?php echo site_url("Admin/Compte_org/afficher")?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Compte Organisateur</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Gestion Invités">
          <a class="nav-link" href="<?php echo site_url("Admin/Gestion_invite/afficher")?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Gestion Invités</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Gestion Evénements">
          <a class="nav-link" href="<?php echo site_url("Admin/Gestion_prog/afficher")?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Gestion Evénements</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Gestion Actualités">
          <a class="nav-link" href="<?php echo site_url("Admin/Gestion_actu/afficher")?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Gestion Actualités</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Gestion FAQ">
          <a class="nav-link" href="<?php echo site_url("Admin/Gestion_FAQ/afficher")?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Gestion FAQ</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
       <li class="nav-item">
         <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
           <i class="fa fa-fw fa-sign-out"></i>Logout</a>
       </li>
     </ul>
  </nav>
