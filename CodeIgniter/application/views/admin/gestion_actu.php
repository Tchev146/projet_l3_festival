<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        Back-Office
      </li>
      <li class="breadcrumb-item active">Gestion Actualités</li>
    </ol>


    <h1><?php echo $titre; ?></h1>
    <br />

    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Liste des actualités
        <a href="<?php echo site_url('Admin/Gestion_actu/ajouter');?>" ><img style="float: right" src="<?php echo base_url();?>style/ic_add_black_48dp_1x.png" ></a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead><tr><td>Titre : </td><td>Contenu : </td><td>Média : </td><td>Auteur : </td><td>Affichage : </td><td>Date : </td></tr></thead>
            <tbody>
              <?php
                foreach ($actu as $key) {
                  if($key['act_visible']=='o'){
                    $visi="Oui";
                  }
                  else{
                    $visi="Non";
                  }
                  echo('<tr><td><a href="'.site_url("Admin/Gestion_actu/modifier/".$key['act_id']).'">'.$key['act_titre'].'</a></td><td>'.$key['act_contenu'].'</td><td>'.$key['act_media'].'</td><td>'.$key['t_organisateur_org_org_pseudo'].'</td><td>'.$visi.'</td><td>'.$key['act_date'].'</td></tr>');
                }
              ?>
            </tbody>
        </table>
      </div>
    </div>
  </div>
