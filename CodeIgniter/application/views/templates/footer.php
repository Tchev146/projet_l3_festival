        	<hr />

		<footer class="site-footer">
			<div class="container">
				<p class="copy">Copyright 2017 - Kevin Kerboit - L3 Ingéniérie Informatique UBO - Brest - All right reserved</p>
			</div>
		</footer> <!-- .site-footer -->

		</div> <!-- #site-content -->

    <script src="<?php echo base_url();?>style/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>style/js/plugins.js"></script>
	  <script src="<?php echo base_url();?>style/js/app.js"></script>


	</div> <!-- #site-content -->
    </body>
</html>
