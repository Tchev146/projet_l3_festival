<?php setlocale (LC_ALL, "fr_FR"); ?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

		<title>Hard Rock's Legends Festival</title>

		<!-- Chargement du style -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>style/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>style/style.css'>
		<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>style/table.css'>
		<style>
			.title_websitename{
				color:#fd5927;
			}

			.error{
	      color:red;
	    }
		</style>
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->
    </head>
    <body class="header-collapse">
	<div id="site-content">
		<header class="site-header">
			<div class="container">
				<a href="index.html" id="branding">
				<h3 class="title_websitename">Hard's Rock's Legends</h3>
				</a> <!-- #branding -->

				<nav class="main-navigation">
					<button type="button" class="toggle-menu"><i class="fa fa-bars"></i></button>
					<ul class="menu">
						<li class="menu-item"><a href="<?php echo site_url("accueil/afficher")?>">Accueil</a></li>
						<li class="menu-item"><a href="<?php echo site_url("invite/afficher")?>">Invité</a></li>
						<li class="menu-item"><a href="<?php echo site_url("programmation/afficher")?>">Programmation</a></li>
						<li class="menu-item"><a href="<?php echo site_url("actualite/afficher")?>">Actualité</a></li>
						<li class="menu-item"><a href="<?php echo site_url("faq/afficher")?>">FAQ</a></li>
						<li class="menu-item"><a href="<?php echo site_url("service/afficher")?>">Service</a></li>
						<li class="menu-item"><a href="<?php echo site_url("objets_trouve/afficher")?>">Objets trouvés</a></li>
						<li class="menu-item"><a href="<?php echo site_url("lieux/afficher")?>">Lieux</a></li>
						<li class="menu-item"><a href="<?php echo site_url("connexion/login")?>">Connexion</a></li>
					</ul> <!-- .menu -->
				</nav> <!-- .main-navigation -->
				<div class="mobile-menu"></div>
			</div>
		</header> <!-- .site-header -->
