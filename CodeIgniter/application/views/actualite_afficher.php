<main class="main-content">
				<div class="fullwidth-block inner-content">
					<div class="container">
						<div class="row">
							<div class="col-md-7">
								<div class="content">
									<br />
									<br />
									<h2 class="entry-title"><?php echo $titre;?></h2>

									<?php foreach ($actu as $key) :?>
									<div class="post">
										<div class="entry-date">
											<?php
											$date = $key['act_date'];
											$mois = strftime("%b", strtotime($date));
											$jour = strftime("%e", strtotime($date));
											echo '<div class="date">'.$jour.'</div>';
											echo '<span class="month">'.$mois.'</span>';
											?>
										</div>
										<div class="featured-image">
											<img src="<?php echo base_url()."".$key['act_media'];?>" alt="">
										</div>
										<h2 class="entry-title"><?php echo $key['act_titre'];?></h2>
										<p><?php echo $key['act_contenu']; ?></p>
										<br />
										<p>Ecrit par : <?php echo $key['t_organisateur_org_org_pseudo']; ?></p>

									</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>
				</div>
</main><!-- .main-content -->
