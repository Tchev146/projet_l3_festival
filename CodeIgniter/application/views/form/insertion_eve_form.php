<script type='text/javascript'>

      function addFields(){
        phpVars = new Array();
        <?php foreach($inv as $var) {
          echo 'phpVars.push("' . $var['inv_nom']. '");';
        };
        ?>

          // Number of inputs to create
          var number = document.getElementById("inv_nb").value;
          // Container <div> where dynamic content will be placed
          var container = document.getElementById("container");
          // Clear previous contents of the container
          while (container.hasChildNodes()) {
              container.removeChild(container.lastChild);
          }
          for (i=0;i<number;i++){

            var newlabel = document.createElement("Label");
            newlabel.setAttribute("for","inv_n" + (i+1));
            newlabel.innerHTML = "Invité "+(i+1)+" : ";
            container.appendChild(newlabel);

              var select = document.createElement("select");
              select.name = "inv_n" + i;
              select.id = "inv_n" + i;
              select.className = "form-control";
              container.appendChild(select);

              for (var j = 0; j < phpVars.length; j++) {
                  var option = document.createElement("option");
                  option.value = phpVars[j];
                  option.text = phpVars[j];
                  select.appendChild(option);
              }

              container.appendChild(select);
              container.appendChild(document.createElement("br"));

          }
      }
  </script>

<?php echo form_open('Admin/Gestion_prog/ajouter/'); ?>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="eve_nom">Nom événement : </label>
      <input class="form-control" id="eve_nom" type="text" name="eve_nom">
      <?php echo form_error('eve_nom', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="eve_desc">Description événement : </label>
      <textarea class="form-control" id="eve_desc" name="eve_desc" ></textarea>
      <?php echo form_error('eve_desc', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div  class="form-group">
  <div class="form-row">
    <div class="col-md-1">
      <label for="inv_nb">Nombre d'invité : </label>
      <input class="form-control" id="inv_nb" name="inv_nb" type="number">
      <a href="#" id="addinvite" onclick="addFields()">Ajouter invité</a>
      <?php echo form_error('inv_nb', '<span class="error">', '</span>');?>
      <br />
      <br />
    </div>
    <div class="col-md-6">
      <div id="container" />
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="lie_nom">Lieu : </label>
      <select class="form-control" id="lie_nom" name="lie_nom">
        <?php foreach ($lie as $key):?>
        <option value="<?php echo $key['lie_nom'];?>"><?php echo $key['lie_nom'];?></option>
        <?php endforeach; ?>
      </select>
      <?php echo form_error('lie_nom', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="eve_datedeb">Date début :</label>
      <input class="form-control" id="eve_datedeb" type="date" name="eve_datedeb" >
      <?php echo form_error('eve_datedeb', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-6">
      <label for="eve_datefin">Date fin :</label>
      <input class="form-control" id="eve_datefin" type="date" name="eve_datefin" >
      <?php echo form_error('eve_datefin', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="eve_heuredeb">Heure début :</label>
      <input class="form-control" id="eve_heuredeb" type="time" name="eve_heuredeb" >
      <?php echo form_error('eve_datedeb', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-6">
      <label for="eve_heurefin">Heure fin :</label>
      <input class="form-control" id="eve_heurefin" type="time" name="eve_heurefin">
      <?php echo form_error('eve_heurefin', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <input class="btn btn-primary btn-block" type="submit" value="Ajouter" />
      <?php if(isset($success)){echo "<span class='success'>".$success."</span>";}?>
    </div>
  </div>
</div>


<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
