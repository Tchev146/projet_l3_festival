<?php echo form_open('Admin/Gestion_actu/ajouter'); ?>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="actu_titre">Titre : </label>
      <input class="form-control" id="actu_titre" type="text" name="actu_titre">
      <?php echo form_error('actu_titre', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="actu_contenue">Contenu : </label>
      <textarea class="form-control" id="actu_contenue" name="actu_contenue"></textarea>
      <?php echo form_error('actu_contenue', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="actu_media">Média : </label>
      <input class="form-control" id="actu_media" type="text" name="actu_media">
      <?php echo form_error('actu_media', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-5">
      <label for="actu_org">Organisateur : </label>
      <select class="form-control" id="actu_org" name="actu_org">
        <?php foreach ($org as $key):?>
        <option value="<?php echo $key['org_pseudo'];?>"><?php echo $key['org_pseudo'];?></option>
        <?php endforeach; ?>
      </select>
      <?php echo form_error('actu_org', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-1">
      <label for="actu_aff">Affichage : </label>
      <select class="form-control" id="actu_aff" name="actu_aff">
        <option value="o">Oui</option>
        <option value="n">Non</option>
      </select>
      <?php echo form_error('actu_aff', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="actu_date">Date :</label>
      <input class="form-control" id="actu_date" type="date" name="actu_date" >
      <?php echo form_error('actu_date', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <input class="btn btn-primary btn-block" type="submit" value="Ajouter" />
      <?php if(isset($success)){echo "<span class='success'>".$success."</span>";}?>
    </div>
  </div>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
