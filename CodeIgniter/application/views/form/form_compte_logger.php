<?php foreach ($org as $key): ?>
<?php echo form_open('Admin/Accueil_admin/afficher'); ?>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="exampleInputName">Prenom : </label>
      <input class="form-control" id="exampleInputName" type="text" name="exampleInputName" aria-describedby="nameHelp" value="<?php echo $key['org_prenom'];?>">
      <?php echo form_error('exampleInputName', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-6">
      <label for="exampleInputLastName">Nom : </label>
      <input class="form-control" id="exampleInputLastName" type="text" name="exampleInputLastName" aria-describedby="nameHelp" value="<?php echo $key['org_nom'];?>">
      <?php echo form_error('exampleInputLastName', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="username">Pseudo : </label>
      <input class="form-control" id="username" type="text" name="username" value="<?php echo $key['org_pseudo'];?>" >
      <?php echo form_error('username', '<span class="error">', '</span>');?>
    </div>

    <div class="col-md-5">
      <label for="password-set">Mot de passe Actuel: </label>
      <input class="form-control" id="password-set" type="password" name="password-set" >
      <?php echo form_error('password-set', '<span class="error">', '</span>');?>
      <?php if(isset($error_pa)){echo "<span class='error'>".$error_pa."</span>";} ?>

    </div>

    <div class="col-md-1">
      <label for="checkbox">Afficher mdp ? </label>
      <br />
      <input type="checkbox" id="checkbox" onchange="document.getElementById('password-set').type = this.checked ? 'text' : 'password'">
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="new-password">Nouveau mot de passe : </label>
      <input class="form-control" id="new-password" name="new-password" type="password" >
      <?php if(isset($error)){echo "<span class='error'>".$error."</span>";} ?>
    </div>
    <div class="col-md-6">
      <label for="new-password-confirm">Confirmer le mot de passe : </label>
      <input class="form-control" id="new-password-confirm" name="new-password-confirm" type="password">
    </div>
  </div>
</div>
<input class="btn btn-primary btn-block" type="submit" value="Modifier" />
<?php if(isset($success)){echo "<span class='success'>".$success."</span>";}?>

<?php echo form_close(); ?>
<?php endforeach; ?>

</div>
</div>
</div>
</div>
