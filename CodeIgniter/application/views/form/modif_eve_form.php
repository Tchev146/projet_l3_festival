<script type='text/javascript'>

      function addOneFields(){
        phpVars = new Array();
        <?php foreach($inv as $var) {
          echo 'phpVars.push("' . $var['inv_nom']. '");';
        };
        ?>

          // Number of inputs to create
          var number = 1;
          var count = document.getElementById('ele').getElementsByTagName('select').length;
          var nb = document.getElementById('container').getElementsByTagName('select').length;
          var nbEle = count + nb;

          // Container <div> where dynamic content will be placed
          var container = document.getElementById("container");
          // Clear previous contents of the container
          while (container.hasChildNodes()) {
              container.removeChild(container.lastChild);
          }
          for (i=count;i<(number+nbEle);i++){

            var newlabel = document.createElement("Label");
            newlabel.setAttribute("for","inv_n" + (i+1));
            newlabel.innerHTML = "Invité "+(i+1)+" : ";
            container.appendChild(newlabel);

              var select = document.createElement("select");
              var nbi = (i+1);
              select.name = "inv_n" +nbi;
              select.id = "inv_n" +nbi;
              select.className = "form-control";
              container.appendChild(select);

              for (var j = 0; j < phpVars.length; j++) {
                  var option = document.createElement("option");
                  option.value = phpVars[j];
                  option.text = phpVars[j];
                  select.appendChild(option);
              }

              container.appendChild(select);
              container.appendChild(document.createElement("br"));

          }
      }

      function suppr(id){
        var elem = document.getElementById(id);
        elem.remove();
      }

  </script>

<?php echo form_open('Admin/Gestion_prog/modifier/'.$eve->eve_id); ?>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="eve_nom">Nom événement : </label>
      <input class="form-control" id="eve_nom" type="text" name="eve_nom" value="<?php echo $eve->eve_nom;?>">
      <?php echo form_error('eve_nom', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="eve_desc">Description événement : </label>
      <textarea class="form-control" id="eve_desc" name="eve_desc" ><?php echo $eve->eve_descriptif;?></textarea>
      <?php echo form_error('eve_desc', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div  class="form-group">
  <div class="form-row">
    <div id="ele" class="col-md-6">
      <?php
        foreach ($inv_eve as $key => $val) {
          echo '<br />';
          echo '<label for="inv_n'.($key+1).'">Invite '.($key+1).' : </label>';
          echo '<select class="form-control" id="inv_n'.($key+1).'" name="inv_n'.($key+1).'">';
          foreach ($inv as $valeur) {
            if($valeur['inv_nom']==$val['inv_nom']){
              echo "<option selected value='".$valeur['inv_nom']."'>".$valeur['inv_nom']."</option>";
            }
            else{
              echo "<option value='".$valeur['inv_nom']."'>".$valeur['inv_nom']."</option>";
            }
          }

          echo '</select>';
          $var = "'inv_n".($key+1)."'";
          echo '<a href="#" id="addinvite" onclick="suppr('.$var.')">Supprimer invité</a>';
          echo '<br />';

        }
      ?>
    </div>
    <div class="col-md-6">
      <div id="container" />
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
<a href="#" id="addinvite" onclick="addOneFields()">Ajouter invité</a>
</div>
</div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="lie_nom">Lieu : </label>
      <select class="form-control" id="lie_nom" name="lie_nom">
        <?php foreach ($lie as $key){
          if($key['lie_nom']==$eve->lie_nom){
            echo "<option selected value='".$key['lie_nom']."'>".$key['lie_nom']."</option>";
          }
          else{
            echo "<option value='".$key['lie_nom']."'>".$key['lie_nom']."</option>";
          }
        }
        ?>
      </select>
      <?php echo form_error('lie_nom', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="eve_datedeb">Date début :</label>
      <input class="form-control" id="eve_datedeb" type="date" name="eve_datedeb" value="<?php echo $eve->eve_datedeb;?>">
      <?php echo form_error('eve_datedeb', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-6">
      <label for="eve_datefin">Date fin :</label>
      <input class="form-control" id="eve_datefin" type="date" name="eve_datefin" value="<?php echo $eve->eve_datefin;?>">
      <?php echo form_error('eve_datefin', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="eve_heuredeb">Heure début :</label>
      <input class="form-control" id="eve_heuredeb" type="time" name="eve_heuredeb" value="<?php echo $eve->eve_heuredeb;?>">
      <?php echo form_error('eve_datedeb', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-6">
      <label for="eve_heurefin">Heure fin :</label>
      <input class="form-control" id="eve_heurefin" type="time" name="eve_heurefin" value="<?php echo $eve->eve_heurefin;?>">
      <?php echo form_error('eve_heurefin', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <input class="btn btn-primary btn-block" type="submit" value="Modifier" />
      <?php if(isset($success)){echo "<span class='success'>".$success."</span>";}?>
    </div>
    <div class="col-md-6">
      <a class="btn btn-primary btn-block" href="<?php echo site_url('Admin/Gestion_prog/suppression/'.$eve->eve_id);?>">Supprimer</a>
    </div>
  </div>
</div>


<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
