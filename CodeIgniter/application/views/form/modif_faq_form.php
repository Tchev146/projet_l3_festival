<?php echo form_open('Admin/Gestion_FAQ/modifier/'.$question->faq_id); ?>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="ques_titre">Titre : </label>
      <input class="form-control" id="ques_titre" type="text" name="ques_titre" value="<?php echo $question->faq_titre;?>">
      <?php echo form_error('ques_titre', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="ques_question">Question : </label>
      <textarea class="form-control" id="ques_question" name="ques_question"><?php echo $question->faq_question ;?></textarea>
      <?php echo form_error('ques_question', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-6">
      <label for="ques_rep">Réponse : </label>
      <textarea class="form-control" id="ques_rep" name="ques_rep"><?php echo $question->faq_reponse;?></textarea>
      <?php echo form_error('ques_rep', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="ques_mail">Adresse Mail : </label>
      <input class="form-control" id="ques_mail" type="text" name="ques_mail" value="<?php echo $question->faq_mail_retour;?>">
      <?php echo form_error('ques_mail', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-5">
      <label for="ques_org">Organisateur : </label>
      <select class="form-control" id="ques_org" name="ques_org">
        <?php foreach ($org as $key){
          if($key['org_pseudo']==$question->t_organisateur_org_org_pseudo){
            echo "<option selected value=".$key['org_pseudo'].">".$key['org_pseudo']."</option>";
          }
          else{
            echo "<option value=".$key['org_pseudo'].">".$key['org_pseudo']."</option>";
          }
        }
        ?>
      </select>
      <?php echo form_error('ques_org', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-1">
      <label for="ques_aff">Affichage : </label>
      <select class="form-control" id="ques_aff" name="ques_aff">
        <?php
          if($question->faq_affichage == 'o'){
            echo "<option selected value='o'>Oui</option>";
            echo "<option value='n'>Non</option>";
          }
          else{
            echo "<option value='o'>Oui</option>";
            echo "<option  selected value='n'>Non</option>";
          }
        ?>
      </select>
      <?php echo form_error('ques_aff', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="ques_num_tic">Numéro de ticket : </label>
      <input class="form-control" id="ques_num_tic" type="number" name="ques_num_tic" value="<?php echo $question->t_ticket_tic_tic_num;?>">
      <?php echo form_error('ques_num_tic', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-6">
      <label for="ques_chaine">Chaine de caractères : </label>
      <input class="form-control" id="ques_chaine" type="text" name="ques_chaine" value="<?php echo $question->t_ticket_tic_tic_chaine_car;?>">
      <?php echo form_error('ques_chaine', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <input class="btn btn-primary btn-block" type="submit" value="Modifier" />
      <?php if(isset($success)){echo "<span class='success'>".$success."</span>";}?>
    </div>
    <div class="col-md-6">
      <a class="btn btn-primary btn-block" href="<?php echo site_url('Admin/Gestion_FAQ/suppression/'.$question->faq_id);?>">Supprimer</a>
    </div>
  </div>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
