<?php echo form_open('Admin/Gestion_actu/modifier/'.$actu->act_id); ?>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="actu_titre">Titre : </label>
      <input class="form-control" id="actu_titre" type="text" name="actu_titre" value="<?php echo $actu->act_titre;?>">
      <?php echo form_error('actu_titre', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="actu_contenue">Contenu : </label>
      <textarea class="form-control" id="actu_contenue" name="actu_contenue"><?php echo $actu->act_contenu ;?></textarea>
      <?php echo form_error('actu_contenue', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="actu_media">Média : </label>
      <input class="form-control" id="actu_media" type="text" name="actu_media" value="<?php echo $actu->act_media;?>">
      <?php echo form_error('actu_media', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-5">
      <label for="actu_org">Organisateur : </label>
      <select class="form-control" id="actu_org" name="actu_org">
        <?php foreach ($org as $key){
          if($key['org_pseudo']==$actu->t_organisateur_org_org_pseudo){
            echo "<option selected value=".$key['org_pseudo'].">".$key['org_pseudo']."</option>";
          }
          else{
            echo "<option value=".$key['org_pseudo'].">".$key['org_pseudo']."</option>";
          }
        }
        ?>
      </select>
      <?php echo form_error('actu_org', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-1">
      <label for="actu_aff">Affichage : </label>
      <select class="form-control" id="actu_aff" name="actu_aff">
        <?php
          if($actu->act_visible == 'o'){
            echo "<option selected value='o'>Oui</option>";
            echo "<option value='n'>Non</option>";
          }
          else{
            echo "<option value='o'>Oui</option>";
            echo "<option  selected value='n'>Non</option>";
          }
        ?>
      </select>
      <?php echo form_error('actu_aff', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="actu_date">Date :</label>
      <input class="form-control" id="actu_date" type="date" name="actu_date" value="<?php echo $actu->act_date;?>">
      <?php echo form_error('actu_date', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <input class="btn btn-primary btn-block" type="submit" value="Modifier" />
      <?php if(isset($success)){echo "<span class='success'>".$success."</span>";}?>
    </div>
    <div class="col-md-6">
      <a class="btn btn-primary btn-block" href="<?php echo site_url('Admin/Gestion_actu/suppression/'.$actu->act_id);?>">Supprimer</a>
    </div>
  </div>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
