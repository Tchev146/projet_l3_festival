<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Hard Rock's Legends : Page de connexion </title>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>style/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>style/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>style/admin/css/sb-admin.css" rel="stylesheet">
  <style>
    .error{
      color:red;
    }
  </style>
</head>
<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Connexion</div>
      <div class="card-body">
          <?php echo form_open('Connexion/login'); ?>

          <div class="form-group">
            <label for="username">Utilisateur :</label>
            <input class="form-control" id="username" type="text" name="username" value="<?php echo set_value('username'); ?>" />
  	        <?php echo form_error('username', '<span class="error">', '</span>');?>
          </div>

          <div class="form-group">
            <label for="password">Mot de Passe : </label>
            <input class="form-control" id="password" type="password" name="password"/>
  	        <?php echo form_error('password', '<span class="error">', '</span>');?>
          </div>

          <input class="btn btn-primary btn-block" type="submit" value="Connexion" />
        <?php echo form_close(); ?>

        <?php
          if(isset($error)){
            echo("<span class='error'>".$error."</span>");
          }
        ?>
      </div>
    </div>
  </div>
<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url();?>style/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>style/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="<?php echo base_url();?>style/admin/vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
