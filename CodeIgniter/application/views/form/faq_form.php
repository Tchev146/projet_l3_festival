
                <h2 class="entry-title">Posez votre question</h2>
                <?php echo form_open('Faq/afficher', 'class="contact-form"'); ?>

                <br />
                <br />

                <label for="faq_titre"> Titre : </label>
                <input type="text" name="faq_titre" value="<?php echo set_value("faq_titre");?>" placeholder="Titre..."/>
                <?php echo form_error('faq_titre', '<span class="error">', '</span>');?>

                <br />
                <br />

                <label for="faq_question"> Question : </label>
                <textarea name="faq_question" value="<?php echo set_value("faq_question");?>" placeholder="Question..." ></textarea>
                <?php echo form_error('faq_question', '<span class="error">', '</span>');?>

                <br />
                <br />

                <label for="faq_mail_retour">Adresse Mail (optionnelle) : </label>
                <input type="email" name="faq_mail_retour" value="<?php echo set_value("faq_mail_retour");?>" placeholder="Adresse Mail..."/>
                <?php echo form_error('faq_mail_retour', '<span class="error">', '</span>');?>

                <br />
                <br />

                <label for="tic_num">Numéro de ticket : </label>
                <input type="number" name="tic_num" value="<?php echo set_value("tic_num");?>" placeholder="Numéros de ticket..." />
                <?php echo form_error('tic_num', '<span class="error">', '</span>');?>

                <br />
                <br />

                <label for="tic_chaine_car">Chaine de caractères : </label>
                <input type="text" name="tic_chaine_car" value="<?php echo set_value("tic_chaine_car");?>" placeholder="Chaine de caractère..."/>
                <?php echo form_error('tic_chaine_car', '<span class="error">', '</span>');?>

                <input type="submit" value="Envoyer" />

                <?php echo form_close();?>
        </div>
        </div>
</main>
