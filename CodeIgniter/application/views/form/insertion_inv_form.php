<?php echo form_open('Admin/Gestion_invite/ajouter/'); ?>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="invi_name">Nom : </label>
      <input class="form-control" id="invi_name" type="text" name="invi_name" >
      <?php echo form_error('invi_name', '<span class="error">', '</span>');?>
    </div>
    <div class="col-md-6">
      <label for="invi_media">Média : </label>
      <input class="form-control" id="invi_media" type="text" name="invi_media" >
      <?php echo form_error('invi_media', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-12">
      <label for="invi_desc">Description : </label>
      <textarea class="form-control" id="invi_desc" name="invi_desc"></textarea>
      <?php echo form_error('invi_desc', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <label for="invi_org">Auteur : </label>
      <select class="form-control" id="invi_org" name="invi_org">
        <?php foreach ($org as $key):?>
        <option value="<?php echo $key['org_pseudo'];?>"><?php echo $key['org_pseudo'];?></option>
        <?php endforeach; ?>
      </select>
      <?php echo form_error('invi_org', '<span class="error">', '</span>');?>
    </div>
  </div>
</div>
<div class="form-group">
  <div class="form-row">
    <div class="col-md-6">
      <input class="btn btn-primary btn-block" type="submit" value="Insertion" />
      <?php if(isset($success)){echo "<span class='success'>".$success."</span>";}?>
    </div>
  </div>
</div>
<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
