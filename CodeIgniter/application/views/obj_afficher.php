<main class="main-content">
				<div class="fullwidth-block inner-content">
					<div class="container">
						<div class="row">
							<div class="col-md-7">
								<div class="content">
									<br />
									<br />
									<h2 class="entry-title"><?php echo $titre;?></h2>

									<?php foreach ($obj as $key) :?>
									<div class="post">
										<div class="entry-date">
											<?php
											$date = $key['obj_date'];
											$mois = strftime("%b", strtotime($date));
											$jour = strftime("%e", strtotime($date));
											echo '<div class="date">'.$jour.'</div>';
											echo '<span class="month">'.$mois.'</span>';
											?>
										</div>
										<h2 class="entry-title"><?php echo $key['obj_intitule'];?></h2>
										<p><?php echo $key['obj_description']; ?></p>
										<br />
										<p>Trouvé près du lieu : <?php echo $key['lie_nom']; ?></p>

									</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>
				</div>
</main><!-- .main-content -->
