<main class="main-content">
				<div class="fullwidth-block gallery">
					<div class="container">
						<div class="content fullwidth">
							<br />
							<br />
							<h2 class="entry-title"><?php echo $titre;?></h2>
						<!--	<div class="filter-links filterable-nav">
								<select class="mobile-filter">
									<option value="*">Show all</option>
									<option value=".concert">Concert</option>
									<option value=".band">Band</option>
									<option value=".stuff">Stuff</option>
								</select>
								<a href="#" class="current" data-filter="*">Show all</a>
								<a href="#" data-filter=".concert">Concert</a>
								<a href="#" data-filter=".band">Band</a>
								<a href="#" data-filter=".stuff">Stuff</a>
							</div>-->
							<div class="filterable-items">
							<?php foreach ($inv as $key) :?>
								<div class="filterable-item concert">
									<?php
									$path = base_url().$key['inv_media'];
									$descinv= site_url("desc_invite/afficher/".$key['inv_id']);
									?>
									<a href="<?php echo $descinv;?>"><figure><img src="<?php echo $path;?>" alt="<?php echo $key['inv_nom']."_image";?> "></figure></a>
								</div>
							<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div> <!-- .testimonial-section -->
</main> <!-- .main-content -->
