
    <div class="hero">
				<div class="slider">
					<ul class="slides">
						<li class="lazy-bg" data-background="<?php echo base_url();?>style/dummy/slide-2.jpg">
							<div class="container">

								<h2 class="slide-title">
                  <?php foreach ($infos as $value) {echo $value['fes_nom'];} ?>
                </h2>

								<h3 class="slide-subtitle">
                  <?php foreach ($infos as $value) {echo $value['fes_lieu'];} ?>
                </h3>

                <p class="slide-desc">
                Du
                <?php foreach ($infos as $value) {echo strftime("%A %e %B %Y", strtotime($value['fes_date_deb']));echo " au "; echo strftime("%A %e %B %Y", strtotime($value['fes_date_fin']));}?>
                <br />
                Horraire :
                <?php foreach ($infos as $value) {echo strftime("%kh%M", strtotime($value['fes_heure_ouvert']));echo " / "; echo strftime("%kh%M", strtotime($value['fes_heure_ferme'])); }?>
                </p>
							</div>
						</li>
					</ul>
				</div>
    </div>
