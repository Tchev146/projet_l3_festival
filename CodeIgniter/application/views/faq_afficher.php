<main class="main-content">
				<div class="fullwidth-block inner-content">
					<div class="container">
						<div class="row">
							<div class="col-md-7">
								<div class="content">
									<br />
									<br />
									<h2 class="entry-title"><?php echo $titre;?></h2>

									<?php
									if(isset($success)){
										echo("<div class='success'>".$success."</div>");
										echo("<br />");
										echo("<br />");
									}
									?>

									<?php foreach ($question as $donnees) :?>
									<div class="post">
										<h2 class="entry-title"><?php echo $donnees['faq_titre'];?></h2>
                    <p>Question : </p>
                    <p><?php echo $donnees['faq_question']; ?></p>
										<br />
                    <p>Réponse : </p>
                    <p><?php echo $donnees['faq_reponse']; ?></p>
                    <br />
										<p>Auteur de la réponse : <?php echo $donnees['t_organisateur_org_org_pseudo']; ?></p>

									</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
