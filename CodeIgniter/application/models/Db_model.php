<?php
class Db_model extends CI_Model {
  public function __construct(){
    $this->load->database();
  }
  public function get_all_compte(){
    $query = $this->db->query("SELECT org_pseudo FROM t_organisateur_org;");
    return $query->result_array();
  }

  public function get_one_compte($pseudo){
    $query = $this->db->query("SELECT * FROM t_organisateur_org WHERE org_pseudo='".$pseudo."';");
    return $query->result_array();
  }

  public function get_info_fest(){
    $query = $this->db->query("SELECT * FROM t_festival_fes;");
    return $query->result_array();
  }

  public function get_all_actualite(){
    $query = $this->db->query("SELECT act_id, act_titre, act_contenu, act_date, act_media, act_visible, t_organisateur_org_org_pseudo FROM t_actualite_act WHERE act_visible='o' ORDER BY act_date DESC;");
    return $query->result_array();
  }

  public function get_ttes_actualite(){
    $query = $this->db->query("SELECT act_id, act_titre, act_contenu, act_date, act_media, act_visible, t_organisateur_org_org_pseudo FROM t_actualite_act;");
    return $query->result_array();
  }

  public function get_one_actu($IDactu){
    $query = $this->db->query("SELECT act_id, act_titre, act_contenu, act_date, act_media, act_visible, t_organisateur_org_org_pseudo FROM t_actualite_act WHERE act_id='".$IDactu."';");
    return $query->row();
  }

  public function get_all_invite(){
    $query = $this->db->query("SELECT inv_id, inv_nom, inv_descriptif, inv_media, t_organisateur_org_org_pseudo FROM t_invite_inv ORDER BY inv_nom;");
    return $query->result_array();
  }

  public function get_one_invite($IDinv){
    $query = $this->db->query("SELECT inv_id, inv_nom, inv_descriptif, inv_media, t_organisateur_org_org_pseudo FROM t_invite_inv WHERE inv_id=".$IDinv.";");
    return $query->row();
  }

  public function get_ressocial($IDinv){
    $query = $this->db->query("SELECT `res_id`, `res_nom`, `res_lien`, `t_invite_inv_inv_id` FROM `t_reseausocial_res` WHERE t_invite_inv_inv_id=".$IDinv.";");
    return $query->result_array();
  }

  public function get_all_evenement(){
    $query = $this->db->query("SELECT * FROM ttes_infos_eve;");
    return $query->result_array();
  }

  public function get_one_evenement($IDeve){
    $query = $this->db->query("SELECT * FROM ttes_infos_eve WHERE eve_id=".$IDeve.";");
    return $query->row();
  }

  public function get_all_faq(){
    $query = $this->db->query("SELECT faq_id, faq_titre, faq_question, faq_reponse, faq_affichage, faq_mail_retour, t_organisateur_org_org_pseudo, t_ticket_tic_tic_chaine_car, t_ticket_tic_tic_num FROM t_faq_faq WHERE faq_affichage='o';");
    return $query->result_array();
  }

  public function get_one_question($IDFAQ){
    $query = $this->db->query("SELECT faq_id, faq_titre, faq_question, faq_reponse, faq_affichage, faq_mail_retour, t_organisateur_org_org_pseudo, t_ticket_tic_tic_chaine_car, t_ticket_tic_tic_num FROM t_faq_faq WHERE faq_id=".$IDFAQ.";");
    return $query->row();
  }

  public function get_all_questions(){
    $query = $this->db->query("SELECT faq_id, faq_titre, faq_question, faq_reponse, faq_affichage, faq_mail_retour, t_organisateur_org_org_pseudo, t_ticket_tic_tic_chaine_car, t_ticket_tic_tic_num FROM t_faq_faq;");
    return $query->result_array();
  }

  public function get_all_lieux(){
    $query = $this->db->query("SELECT lie_id, lie_nom, lie_coordonnees_x, lie_coordonnes_y FROM t_lieux_lie ORDER BY lie_nom ASC;");
    return $query->result_array();
  }

  public function get_all_service(){
    $query = $this->db->query("SELECT ser_id, ser_type, ser_nom, t_lieux_lie_lie_id FROM t_service_ser ORDER BY ser_nom ASC;");
    return $query->result_array();
  }

  public function get_all_obj(){
    $query = $this->db->query("SELECT obj_id, obj_intitule, obj_description, obj_date, t_lieux_lie_lie_id, lie_nom FROM t_objtrouve_obj INNER JOIN t_lieux_lie ON t_lieux_lie_lie_id=lie_id ORDER BY obj_intitule ASC;");
    return $query->result_array();
  }

  public function check_compte($compte, $mdp){
    $query = $this->db->query("SELECT * FROM compte WHERE pseudo='".$compte."' AND password='".$mdp."';");
    return $query->result_array();
  }

  public function add_faq($data){
    $this->db->insert('t_faq_faq', $data);
  }

  public function check_num_tic($num){
    $query = $this->db->query("SELECT tic_num FROM t_ticket_tic WHERE tic_num=".$num.";");
    return $query->result_array();
  }

  public function check_num_char_tic($num, $char){
    $query = $this->db->query("SELECT tic_num, tic_chaine_car FROM t_ticket_tic WHERE tic_num=".$num." AND tic_chaine_car LIKE '".$char."';");
    return $query->result_array();
  }

  public function update_org($donnes, $pseudo){
    $this->db->where('org_pseudo', $pseudo);
    $this->db->update('t_organisateur_org', $donnes);
  }

  public function update_actu($donnes, $IDactu){
    $this->db->where('act_id', $IDactu);
    $this->db->update('t_actualite_act', $donnes);
  }

  public function update_inv($donnes, $IDinv){
    $this->db->where('inv_id', $IDinv);
    $this->db->update('t_invite_inv', $donnes);
  }

  public function update_faq($donnes, $IDfaq){
    $this->db->where('faq_id', $IDfaq);
    $this->db->update('t_faq_faq', $donnes);
  }

  public function select_id_lie($Nomlieu){
    $this->db->where('lie_nom', $Nomlieu);
    $this->db->select('lie_id');
    $query = $this->db->get('t_lieux_lie');
    return $query->row();
  }

  public function update_prog($donnes, $IDeve){
    $this->db->where('eve_id', $IDeve);
    $this->db->update('t_evenement_eve', $donnes);
  }

  public function get_inv_in_eve($IDeve){
    $this->db->where('eve_id', $IDeve);
    $this->db->select('inv_nom');
    $query = $this->db->get('ttes_infos_eve');
    return $query->result_array();
  }

  public function select_id_inv($Nominv){
    $this->db->where('inv_nom', $Nominv);
    $this->db->select('inv_id');
    $query = $this->db->get('t_invite_inv');
    return $query->row();
  }

  public function select_id_eve($Nomeve){
    $this->db->where('eve_nom', $Nomeve);
    $this->db->select('eve_id');
    $query = $this->db->get('t_evenement_eve');
    return $query->row();
  }

  public function update_participe($IDeve, $IDinv, $prec){

  }

  public function insert_actu($donnes){
    $this->db->insert('t_actualite_act', $donnes);
  }

  public function delete_actu($IDactu){
    $this->db->where('act_id', $IDactu);
    $this->db->delete('t_actualite_act');
  }

  public function insert_inv($donnes){
    $this->db->insert('t_invite_inv', $donnes);
  }

  public function delete_inv($IDinv){
    $this->db->query('CALL suppr_invite('.$IDinv.')');
  }

  public function delete_faq($IDfaq){
    $this->db->where('faq_id', $IDfaq);
    $this->db->delete('t_faq_faq');
  }

  public function insert_prog($donnes){
    $this->db->insert('t_evenement_eve', $donnes);
  }

  public function insert_participe($IDeve, $IDinv){
    $this->db->set('t_invite_inv_inv_id', $IDinv);
    $this->db->set('t_evenement_eve_eve_id', $IDeve);
    $this->db->insert('participe');
  }

  public function delete_paticipe($IDeve){
    $this->db->where('t_evenement_eve_eve_id', $IDeve);
    $this->db->delete('participe');
  }

  public function get_nb_inv_par_eve($IDeve){
    $query = $this->db->query("SELECT `nb_invite_eve`(".$IDeve.") AS `nb_invite_eve`;");
    return $query->row();
  }

}

?>
